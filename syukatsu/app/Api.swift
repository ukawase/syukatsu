//
//  Api.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Api: NSObject {
   
    class func url(path:String)->String{
        return API_ENDPOINT + "/" + path
    }
}
