//
//  TweetListViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class TweetListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {

    private var tweetList:TweetList?
    private let tableView = UITableView(frame: SUtil.headerContentsFrame())
    private let refreshControl = UIRefreshControl()
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, TAB_HEIGHT+64, 0)
        
        refreshControl.addTarget(self, action: "requestRefresh:", forControlEvents: UIControlEvents.ValueChanged)
        tableView.addSubview(refreshControl)
        
        self.view.addSubview(tableView)
    }
    
    func requestRefresh(ctl:UIRefreshControl){
        println("requestRefresh")
        self.reload()
    }
    
    override func reload() {
        Model.sharedInstance.loadTweet { (tweetList) -> () in
            self.tweetList = tweetList
            self.tableView.reloadData()
            self.hideLoading()
            self.refreshControl.endRefreshing()
        }
    }
    
    override func getHeaderTitle() -> String {
        return "就活ツイート"
    }
    
    private func isAdIndex(indexPath:NSIndexPath)->Bool{
        if !AloeUtil.isJaDevice(){
            return false
        }
        if SUtil.isNoAd(){
            return false
        }
        return indexPath.row % NATIVE_AD_LIST_RATE == 0 && indexPath.row != 0
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if isAdIndex(indexPath){
            return 104
        }
        
        let tweet:TweetModel = tweetList!.get(indexPath.row)
        
        return TweetCell.height(tweet.txt)
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tweetList == nil{
            return 0
        }
        return tweetList!.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // ad
        if self.isAdIndex(indexPath){
            let adCellIdentifier:String = "adCellIdentifier"
            var cell:AdCell? = tableView.dequeueReusableCellWithIdentifier(adCellIdentifier) as AdCell?
            
            if cell == nil{
                cell = AdCell(style:UITableViewCellStyle.Default, reuseIdentifier:adCellIdentifier)
                cell!.setup()
            }
            
            cell!.reload()
            
            return cell!
        }
        
        let cellIdentifier:String = "tweetCellIdentifier"
        
        var cell:TweetCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as TweetCell?
        if cell == nil {
            cell = TweetCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let tweet:TweetModel = tweetList!.get(indexPath.row)
        cell!.reload(tweet)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // ad
        if isAdIndex(indexPath){
            let cell:AdCell = tableView.cellForRowAtIndexPath(indexPath) as AdCell
            let ad:NativeAd = cell.tap()
            let vc = AppViewController()
            vc.setup(ad)
            
            self.navigationController!.pushViewController(vc, animated: true)
            return
        }
        
        let cell:TweetCell = tableView.cellForRowAtIndexPath(indexPath) as TweetCell
        cell.tap()
        
        let tweet:TweetModel = tweetList!.get(indexPath.row)
        println("\(tweet.name)")
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(tweet.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("ツイッター：" + tweet.txt)
        
    }
    
}
