//
//  BaseViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class BaseViewController: AloeViewController, UIScrollViewDelegate{

    func reload(){
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.automaticallyAdjustsScrollViewInsets = false
        
    }
    
    func getHeaderTitle()->String{
        return ""
    }

    // MARK: UIScrollViewDelegate
    
    private var currentY:CGFloat = 0
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let speed:CGFloat = scrollView.contentOffset.y - currentY
        
        currentY = scrollView.contentOffset.y
        
        if speed > 5{
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_DOWN_EVENT)
        }else if speed < -5{
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_UP_EVENT)
        }
    }
}
