//
//  AppViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/31.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit


class AppViewController: BaseViewController {
   
    let titleLabel = UILabel()
    let detailLabel = UILabel()
    let descriptionLabel = UILabel()
    let ratingLabel = UILabel()
    let linkButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    let imageView = UIImageView()
    let sv = UIScrollView()
    var nativeAd:NativeAd?
    let screenshotSv = UIScrollView()
    
    func setup(nativeAd:NativeAd){
        self.nativeAd = nativeAd
        
        self.view.backgroundColor = UIColor.whiteColor()
        self.setContents()
        self.setBackButton()
        self.loadAppStoreApi(nativeAd)
        
        // statusbar
        let statusBar = UIView(frame: CGRectMake(0, 0, 320, 20))
        statusBar.backgroundColor = UIColor.blackColor()
        statusBar.alpha = 0.3
        self.view.addSubview(statusBar)
    }
    
    private func setBackButton(){
        let backButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        backButton.frame = CGRectMake(260, AloeDeviceUtil.windowHeight()-105, 45, 45)
        backButton.setImage(UIImage(named: "adBackButton"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: "tapBack", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(backButton)
    }
    
    func tapBack(){
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    private func setContents(){
        sv.frame = CGRectMake(0, 0, 320, AloeDeviceUtil.windowHeight()-50)
        self.view.addSubview(sv)
        
        imageView.frame = CGRectMake(60, 40, 200, 101);
        imageView.contentMode = UIViewContentMode.ScaleAspectFit;
        imageView.userInteractionEnabled = true
        imageView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapImage"))
        sv.addSubview(imageView)
        
        var y:CGFloat = 40 + 101 + 20;
        
        titleLabel.frame = CGRectMake(10, y, 300, 20)
        titleLabel.numberOfLines = 2
        titleLabel.font = UIFont.boldSystemFontOfSize(20.0)
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.text = nativeAd!.title
        sv.addSubview(titleLabel)
        y += titleLabel.frame.size.height + 15;
        
        ratingLabel.font = UIFont.systemFontOfSize(13.0)
        ratingLabel.numberOfLines = 2
        sv.addSubview(ratingLabel)
        ratingLabel.frame = CGRectMake(10, y, 300, 40);
        y += 40+15;
        
        screenshotSv.frame = CGRectMake(0, y, 320, 372)
        screenshotSv.backgroundColor = UIColorFromHex(0xeeeeee);
        var line = UIView(frame: CGRectMake(-200, 0, 1000, 0.5))
        line.backgroundColor = UIColorFromHex(0xcccccc);
        screenshotSv.addSubview(line)
        line = UIView(frame: CGRectMake(-200, 371.5, 1000, 0.5))
        line.backgroundColor = UIColorFromHex(0xcccccc);
        screenshotSv.addSubview(line)
        
        y += 372 + 15;
        
        descriptionLabel.frame = CGRectMake(10, y, 300, 10000);
        descriptionLabel.numberOfLines = 0
        descriptionLabel.text = nativeAd!.content
        descriptionLabel.sizeToFit()
        sv.addSubview(descriptionLabel)
        y += descriptionLabel.frame.size.height + 15;
        
        detailLabel.frame = CGRectMake(10, y, 300, 10000);
        detailLabel.numberOfLines = 0
        detailLabel.text = nativeAd!.detail
        detailLabel.sizeToFit()
        sv.addSubview(detailLabel)
        y += detailLabel.frame.size.height + 15;
        
        linkButton.setImage(UIImage(named: "download"), forState: UIControlState.Normal)
        linkButton.setImage(UIImage(named: "downloadOn"), forState: UIControlState.Highlighted)
        linkButton.addTarget(self, action: "tapButton", forControlEvents: UIControlEvents.TouchUpInside)
        linkButton.frame = CGRectMake(50, y, 220, 44);
        y += linkButton.frame.size.height + 15 + 10 + 45;
        sv.addSubview(linkButton)
        
        sv.addSubview(screenshotSv)
        sv.contentSize = CGSizeMake(320, y);
        
        let imageUrl:String = nativeAd!.hasBanner() ? nativeAd!.bannerUrl! : nativeAd!.iconUrl
        
        AloeImageCache.loadImage(imageUrl, callback: { (image, url, useCache) -> () in
            self.imageView.image = image
        }) { (error) -> () in
            
        }
    }
    
    func tapImage(){
        AloeUtil.pain(imageView)
        self.openApp()
    }
    
    func tapButton(){
        self.openApp()
    }
    
    private func loadAppStoreApi(nativeAd:NativeAd){
        AloeUtil.loadStoreApp(nativeAd.appStoreId, callback:{(storeApp:StoreApp)->() in
            self.ratingLabel.text = "【評価：\(Int(storeApp.currentRating))】 \n\(storeApp.currentRatingCount)件の評価(バージョン\(storeApp.currentVersion))"
            
            var index:Int = 0
            for url in storeApp.screenshotList{
                self.loadScreenshot(url, index: index)
                index++
            }
            self.screenshotSv.contentSize = CGSizeMake(CGFloat((index * 210) + 10), 372);
        })
    }
    
    private func loadScreenshot(url:String, index:Int){
        let frame:CGRect = CGRectMake(CGFloat((index * 210)+10), 10, 200, 352);
        
        let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
        indicator.frame = frame
        screenshotSv.addSubview(indicator)
        indicator.startAnimating()
        
        let iv = UIImageView(frame: frame)
        iv.contentMode = UIViewContentMode.ScaleAspectFit
        iv.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tapScsho:"))
        iv.userInteractionEnabled = true
        
        screenshotSv.addSubview(iv)
        AloeImageCache.loadImage(url, callback: { (image, url, useCache) -> () in
            iv.image = image
        }) { (error) -> () in
            
        }
    }
    
    func tapScsho(reco:UIGestureRecognizer){
        AloeUtil.pain(reco.view!)
        self.openApp()
    }
    
    private func openApp(){
        if nativeAd!.hasBanner(){
            SUtil.trackEvent("largeNativeAd", action:"openApp", label:nativeAd!.title)
        }else{
            SUtil.trackEvent("smallNativeAd", action:"openApp", label:nativeAd!.title)
        }
        AloeUtil.openBrowser(nativeAd!.linkUrl)
    }
}
