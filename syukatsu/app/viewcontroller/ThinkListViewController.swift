//
//  ThinkListViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class ThinkListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var currentList:ArticleList = ArticleList()
    private var thinkList:ArticleList = ArticleList()
    private var worryList:ArticleList = ArticleList()
    private let tableView = UITableView(frame: SUtil.headerContentsFrame())
    private let pickupButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    private let worryButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, TAB_HEIGHT+64, 0)
        
        self.view.addSubview(tableView)
    }
    
    func setHeader(header:Header){
        pickupButton.frame = CGRectMake(62, 27, 98, 29)
        pickupButton.setImage(UIImage(named: "pickupOff"), forState: UIControlState.Normal)
        pickupButton.setImage(UIImage(named: "pickupOn"), forState: UIControlState.Selected)
        pickupButton.addTarget(self, action: "tapPickup:", forControlEvents: UIControlEvents.TouchUpInside)
        pickupButton.selected = true
        header.getView().addSubview(pickupButton)
        
        worryButton.frame = CGRectMake(160, 27, 98, 29)
        worryButton.setImage(UIImage(named: "worryOff"), forState: UIControlState.Normal)
        worryButton.setImage(UIImage(named: "worryOn"), forState: UIControlState.Selected)
        worryButton.addTarget(self, action: "tapWorry:", forControlEvents: UIControlEvents.TouchUpInside)
        header.getView().addSubview(worryButton)
    }

    override func reload() {
        thinkList = Model.sharedInstance.thinkList()
        worryList = Model.sharedInstance.worryList()
        
        currentList =  pickupButton.selected ? thinkList : worryList
        
        tableView.reloadData()
    }
    
    func tapPickup(button:UIButton){
        if button.selected{
            return
        }
        
        pickupButton.selected = true
        worryButton.selected = false
        currentList = thinkList
        self.updateAnimation()
    }
    
    func tapWorry(button:UIButton){
        if button.selected{
            return
        }
        
        pickupButton.selected = false
        worryButton.selected = true
        currentList = worryList
        self.updateAnimation()
    }
    
    private func updateAnimation(){
        AloeTweenChain().add(0.2, ease: AloeEase.InCirc, progress:{ (val) -> () in
            self.view.alpha = 1.0-val
        }).call({()->()in
            self.tableView.reloadData()
        }).add(0.2, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.view.alpha = val
        }).execute()
    }
    
    override func getHeaderTitle() -> String {
        return ""
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 84
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentList.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier:String = "thinkCellIdentifier"
        
        var cell:ThinkCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as ThinkCell?
        if cell == nil {
            cell = ThinkCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let article:Article = currentList.get(indexPath.row)
        cell!.reload(article)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell:ThinkCell = tableView.cellForRowAtIndexPath(indexPath) as ThinkCell
        cell.tap()
        
        let article:Article = currentList.get(indexPath.row)
        println("\(article.title)")
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(article.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("トピック：" + article.title)
    }
    
}
