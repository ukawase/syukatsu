//
//  WebContentsViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit
import Social

class WebContentsViewController: BaseViewController, UIWebViewDelegate {

    let webView = UIWebView(frame: SUtil.contentsFrame())
    private let menuView:UIView = UIView(frame: CGRectMake(0, AloeDeviceUtil.windowHeight()-64-50+20, AloeDeviceUtil.windowWidth(), 50))
    var initialLoad:Bool = true
    private let webNextButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    private let webBackButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    
    func setup(){
        webView.scrollView.contentInset = UIEdgeInsetsMake(20, 0, 50, 0)
        webView.delegate = self
        webView.scrollView.delegate = self
        self.view.addSubview(webView)
        
        // statusbar
        let statusBar = UIView(frame: CGRectMake(0, 0, 320, 20))
        statusBar.backgroundColor = UIColor.blackColor()
        statusBar.alpha = 0.3
        self.view.addSubview(statusBar)
        
        AloeEventUtil.addGlobalEventListener(self, selector: "hideMenu", name: ON_SCROLL_DOWN_EVENT)
        AloeEventUtil.addGlobalEventListener(self, selector: "showMenu", name: ON_SCROLL_UP_EVENT)
        
        self.setMenu()
        self.view.addSubview(IconAdView.sharedInstance.getView())
        IconAdView.sharedInstance.staticShow()
    }
    
    private func setMenu(){
        //        menuView = [[UIView alloc] initWithFrame:CGRectMake(0, [VknDeviceUtil windowHeight]-64-50, 320, 50)];
        self.view.addSubview(menuView)
        let bgView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 50))
        bgView.backgroundColor = UIColorFromHex(0xf7f7f6)
        menuView.addSubview(bgView)
        
        menuView.transform = CGAffineTransformMakeTranslation(0, GlobalMenuTy)
        
        let margin:CGFloat = (AloeDeviceUtil.windowWidth() - 320) / 4
        
        var buttonFrame = CGRectMake(0, 3, 64, 44)
        let backButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        backButton.setImage(UIImage(named: "backButton"), forState: UIControlState.Normal)
        backButton.addTarget(self, action: "tapBack", forControlEvents: UIControlEvents.TouchUpInside)
        backButton.frame = buttonFrame
        menuView.addSubview(backButton)
        buttonFrame.origin.x += 64 + margin
        
        webBackButton.frame = buttonFrame
        webBackButton.setImage(UIImage(named: "webBackButton"), forState: UIControlState.Normal)
        webBackButton.addTarget(self, action: "tapWebBack", forControlEvents: UIControlEvents.TouchUpInside)
        webBackButton.enabled = false
        menuView.addSubview(webBackButton)
        buttonFrame.origin.x += 64 + margin
        
        webNextButton.frame = buttonFrame
        webNextButton.setImage(UIImage(named: "webNextButton"), forState: UIControlState.Normal)
        webNextButton.addTarget(self, action: "tapWebNext", forControlEvents: UIControlEvents.TouchUpInside)
        webNextButton.enabled = false
        menuView.addSubview(webNextButton)
        buttonFrame.origin.x += 64 + margin
        
        let reloadButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        reloadButton.frame = buttonFrame
        reloadButton.setImage(UIImage(named: "webReloadButton"), forState: UIControlState.Normal)
        reloadButton.addTarget(self, action: "tapWebReload", forControlEvents: UIControlEvents.TouchUpInside)
        menuView.addSubview(reloadButton)
        buttonFrame.origin.x += 64 + margin
        
        if AloeDeviceUtil.osVersion() < 8{
            return
        }
        let shareButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        shareButton.frame = buttonFrame
        shareButton.setImage(UIImage(named: "webShareButton"), forState: UIControlState.Normal)
        shareButton.addTarget(self, action: "tapWebShare", forControlEvents: UIControlEvents.TouchUpInside)
        menuView.addSubview(shareButton)
        buttonFrame.origin.x += 64
    }
    
    func hideMenu(){
        if menuView.transform.ty != 0{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.None) { (val) -> () in
            let y:CGFloat = TAB_HEIGHT * val
            self.menuView.transform = CGAffineTransformMakeTranslation(0, y)
        }
    }
    
    func showMenu(){
        if menuView.transform.ty != TAB_HEIGHT{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.None) { (val) -> () in
            let y:CGFloat = TAB_HEIGHT - (TAB_HEIGHT * val)
            self.menuView.transform = CGAffineTransformMakeTranslation(0, y)
        }
    }
    
    func tapBack(){
        self.navigationController!.popViewControllerAnimated(true)
    }
    
    func tapWebBack(){
        if webView.canGoBack{
            initialLoad = true
            self.showLoading()
            webView.goBack()
        }
    }
    
    func tapWebNext(){
        if webView.canGoForward{
            initialLoad = true
            self.showLoading()
            webView.goForward()
        }
    }
    
    func tapWebReload(){
        initialLoad = true
        self.showLoading()
        webView.reload()
    }
    
    func tapWebShare(){
        
        let title:String = webView.stringByEvaluatingJavaScriptFromString("document.title")!
        let url:String = webView.stringByEvaluatingJavaScriptFromString("document.URL")!
        
        let sheet:UIAlertController = UIAlertController(title: "シェアする", message: title, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        // さふぁり
        sheet.addAction(UIAlertAction(title: "Safariで開く", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            AloeUtil.openBrowser(url)
        }))
        
        // line
        sheet.addAction(UIAlertAction(title: "LINE", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let str:String = url.stringByAddingPercentEscapesUsingEncoding(NSUTF8StringEncoding)!
            let lineUrlString = "line://msg/text/" + str
            AloeUtil.openBrowser(lineUrlString)
        }))
        
        // Twitter
        sheet.addAction(UIAlertAction(title: "Twitter", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let vc:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
            vc.setInitialText(title)
            vc.addURL(NSURL(string: url))
            self.presentViewController(vc, animated: true, completion: { () -> Void in
                
            })
        }))
        
        // Facebook
        sheet.addAction(UIAlertAction(title: "Facebook", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let vc:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            vc.setInitialText(title)
            vc.addURL(NSURL(string: url))
            self.presentViewController(vc, animated: true, completion: { () -> Void in
                
            })
 
        }))
        
        // コピー
        sheet.addAction(UIAlertAction(title: "URLをコピー", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let pasteboard:UIPasteboard = UIPasteboard.generalPasteboard()
            pasteboard.setValue(url, forPasteboardType: "public.text")
            self.showToast("コピーしました")
        }))
        
        sheet.addAction(UIAlertAction(title: "記事の問題を報告", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let c:AloeURLLoadCommand = AloeURLLoadCommand(url_: Api.url("ihan.php?title=test"))
            c.execute()
            let alert:UIAlertController = UIAlertController(title: "報告しました", message: "ご協力ありがとうございます", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (actioin) -> Void in
                
            }))
            self.presentViewController(alert, animated: true, completion: { () -> Void in
                
            })
            
        }))
        sheet.addAction(UIAlertAction(title: "キャンセル", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            
        }))
        
        self.presentViewController(sheet, animated: true) { () -> Void in
            
        }
    }
    
    func loadUrl(url:String){
        
        self.showLoading()
        webView.loadRequest(NSURLRequest(URL: NSURL(string: url)!))
        
    }
    
    override func showLoading() {
        super.showLoading()
    }
    
    override func hideLoading() {
        super.hideLoading()
        AloeThreadUtil.wait(2.5, { (data) -> () in
            IconAdView.sharedInstance.hide()
        })
    }

    func webViewDidFinishLoad(webView: UIWebView) {
        if initialLoad{
            self.hideLoading()
//            webView.scrollView.contentInset = UIEdgeInsetsMake(20, 0, TAB_HEIGHT, 0)
        }
        initialLoad = false
        
        webBackButton.enabled = webView.canGoBack /*&& (!isYoutube || clickCount != 0)*/
        webNextButton.enabled = webView.canGoForward
    }
    
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if navigationType == UIWebViewNavigationType.LinkClicked{
            initialLoad = true
            IconAdView.sharedInstance.show()
            self.showLoading()
        }
        
        return true
    }
    
    deinit{
        println("webViewDeinit")
        webView.removeFromSuperview()
        webView.delegate = nil
        AloeEventUtil.removeGlobalEventListener(self)
    }
}
