//
//  TplListViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class InternListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource  {

    var internList:InternList = InternList()
    private let tableView = UITableView(frame: SUtil.headerContentsFrame())
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, TAB_HEIGHT+64, 0)
        self.view.addSubview(tableView)
    }
    
    override func reload() {
        internList = Model.sharedInstance.internList()
        tableView.reloadData()
    }
    
    override func getHeaderTitle() -> String {
        return "インターン情報"
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 190
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return internList.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cellIdentifier:String = "internCellIdentifier"
        
        var cell:InternCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as InternCell?
        if cell == nil {
            cell = InternCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let intern:Intern = internList.get(indexPath.row)
        cell!.reload(intern)
        
        return cell!
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell:InternCell = tableView.cellForRowAtIndexPath(indexPath) as InternCell
        cell.tap()
        
        let intern:Intern = internList.get(indexPath.row)
        println("\(intern.title)")
        
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(intern.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("インターン：" + intern.title)
    }

}
