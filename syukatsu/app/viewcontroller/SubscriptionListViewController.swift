//
//  SubscriptionListViewController.swift
//  nich
//
//  Created by kawase yu on 2014/11/15.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

typealias SubscriptionListViewHideCallback = ()->()

private let SubscriptionCellIdentifier = "SubscriptionCell"
private let SubscriptionAdCellIdentifier = "SubscriptionAdCellIdentifier"
private let SubscriptionInfoCellIdentifier = "SubscriptionInfoCellIdentifier"

class SubscriptionListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private let tableView:UITableView = UITableView(frame: AloeDeviceUtil.windowFrame())
    private var siteList:SiteList?
    private var adList:NativeAdList?
    private let purchaseCell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: "purhcaseCellIdentifer")
    var hideCallback:SubscriptionListViewHideCallback?
    
    func setup(hideCallback:SubscriptionListViewHideCallback){
        self.hideCallback = hideCallback
        self.setPurhcaseCell()
        self.setTableView()
        self.setHeader()
        self.setCloseButton()
        
        self.view.transform = CGAffineTransformMakeTranslation(0, AloeDeviceUtil.windowHeight())
    }
    
    private func setPurhcaseCell(){
        purchaseCell.textLabel.text = "広告削除"
        purchaseCell.accessoryType = UITableViewCellAccessoryType.None
        purchaseCell.detailTextLabel!.text = "-"
        purchaseCell.selectionStyle = UITableViewCellSelectionStyle.None
//        purchaseCell.detailTextLabel!.text = "円"
    }
    
    private func setTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.contentInset = UIEdgeInsetsMake(74, 0, 0, 0)
        self.view.addSubview(tableView)
    }
    
    private func setHeader(){
        let header:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 74))
        let bg:UIView = UIView(frame: header.frame)
        bg.backgroundColor = UIColor.whiteColor()
        bg.alpha = 0.8
        header.addSubview(bg)
        
        let label:UILabel = UILabel(frame: CGRectMake(0, 25, AloeDeviceUtil.windowWidth(), 44))
        label.textAlignment = NSTextAlignment.Center
        label.text = "設定"
        header.addSubview(label)
        
        self.view.addSubview(header)
    }
    
    private func setCloseButton(){
        
        let buttonWidth:CGFloat = 80
        let button:UIButton = UIButton(frame: CGRectMake(AloeDeviceUtil.windowWidth()-buttonWidth, 26, buttonWidth, 44))
        button.setTitleColor(UIColorFromHex(0x333333), forState: UIControlState.Normal)
        button.setTitle("閉じる", forState: UIControlState.Normal)
        button.titleLabel!.font = UIFont.boldSystemFontOfSize(16.0)
        button.addTarget(self, action: "tapClose", forControlEvents: UIControlEvents.TouchUpInside)
        self.view.addSubview(button)
    }
    
    func tapClose(){
        self.hide()
    }
    
    override func reload(){
        siteList = Model.sharedInstance.siteList()
        tableView.reloadData()
    }
    
    func show(){
        adList = Ad.sharedInstance.getIconAdList()
        adList!.shuffle()
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.Default, animated: true)
        AloeTween.doTween(0.3, ease: AloeEase.OutCirc) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, AloeDeviceUtil.windowHeight() - AloeDeviceUtil.windowHeight()*val)
        }
        
        tableView.reloadData()
        
        
        // 課金
        if SUtil.isNoAd(){
            purchaseCell.detailTextLabel!.text = "購入済み"
            return
        }
        Purchase.productInfo(PURCHASE_NOAD, callback: { (product:SKProduct) -> () in
            self.purchaseCell.detailTextLabel!.text = product.price.stringValue + "円"
            self.purchaseCell.selectionStyle = UITableViewCellSelectionStyle.Gray
        })
    }
    
    func hide(){
        self.showLoading()
        
        Model.sharedInstance.setNoSubscriptionSiteList(siteList!)
        
        AloeThreadUtil.wait(0.2, block: { () -> () in
            self.hideCallback!()
            UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: true)
        })
        AloeThreadUtil.wait(0.4, block: { () -> () in
            self.hideLoading()
            AloeTweenChain().add(0.2, ease: AloeEase.InCirc, progress:{(val) -> () in
                self.view.transform = CGAffineTransformMakeTranslation(0, AloeDeviceUtil.windowHeight()*val)
            }).call({ () -> () in
                self.view.removeFromSuperview()
            }).execute()
        })
    }
    
    // MARK: UITableViewDelegate, DataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        
        // 当アプリについて
        if indexPath.section == 0{
            return 44
        }
        
        // 無料アプリ
        if indexPath.section == 2{
            return 70
        }
        
        // 表示設定
        return 44
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // 当アプリについて
        if indexPath.section == 0{
            
            if indexPath.row == 2{
                return purchaseCell
            }
            
            var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(SubscriptionInfoCellIdentifier) as UITableViewCell?
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.Value1, reuseIdentifier: SubscriptionInfoCellIdentifier)
            }
            
            if indexPath.row == 0{
                cell!.textLabel.text = "バージョン"
                cell!.detailTextLabel!.text = NSBundle.mainBundle().infoDictionary?["CFBundleShortVersionString"] as? String
                cell!.accessoryType = UITableViewCellAccessoryType.None
                cell!.selectionStyle = UITableViewCellSelectionStyle.None
            }else if indexPath.row == 1{
                cell!.textLabel.text = "レビューを書く"
                cell!.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
                cell!.selectionStyle = UITableViewCellSelectionStyle.Gray
            }else if indexPath.row == 3{
                cell!.textLabel.text = "リストア"
                cell!.accessoryType = UITableViewCellAccessoryType.None
                cell!.selectionStyle = UITableViewCellSelectionStyle.Gray
            }
            
            return cell!
        }
        
        // 無料アプリ
        if indexPath.section == 2{
//            SubscriptionAdCellIdentifier
            var cell:UITableViewCell? = tableView.dequeueReusableCellWithIdentifier(SubscriptionAdCellIdentifier) as UITableViewCell?
            if(cell == nil){
                cell = UITableViewCell(style: UITableViewCellStyle.Subtitle, reuseIdentifier: SubscriptionAdCellIdentifier)
            }
            cell!.detailTextLabel!.numberOfLines = 3
            cell!.detailTextLabel!.lineBreakMode = NSLineBreakMode.ByWordWrapping
            let ad:NativeAd = adList!.get(indexPath.row)
            cell!.textLabel.text = ad.title
            cell!.detailTextLabel!.text = ad.content
            
            return cell!
        }
        
        // 表示設定
        var cell:SubscriptionCell? = tableView.dequeueReusableCellWithIdentifier(SubscriptionCellIdentifier) as SubscriptionCell?
        if(cell == nil){
            cell = SubscriptionCell(style: UITableViewCellStyle.Default, reuseIdentifier: SubscriptionCellIdentifier)
            cell!.setup()
        }
        
        let site:Site = siteList!.get(indexPath.row)
        cell!.reload(site)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if section == 0{
            return "当アプリについて"
        }else if section == 2{
            return "無料アプリ"
        }
        
        return "表示設定"
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
        
        if indexPath.section == 0{
            if indexPath.row == 1{
                AloeUtil.openBrowser("https://itunes.apple.com/us/app/id954170365?l=ja&ls=1&mt=8")
            }
            else if indexPath.row == 2{
                if SUtil.isNoAd(){ // 購入済み
                    return
                }
                self.showLoading()
                PFPurchase.buyProduct(PURCHASE_NOAD, block: { (error:NSError!) -> Void in
                    self.hideLoading()
                    if error != nil{
                        println("error:\(error)")
                        return
                    }
                    
                    SUtil.purchasedNoAd()
                    println("purchased!!!")
                })
            }else if indexPath.row == 3{
                PFPurchase.addObserverForProduct(PURCHASE_NOAD, block: { (transaction:SKPaymentTransaction!) -> Void in
                    if transaction.payment.productIdentifier == PURCHASE_NOAD{
                        SUtil.purchasedNoAd()
                        self.purchaseCell.detailTextLabel!.text = "購入済み"
                    }
                })
                PFPurchase.restore()
            }
        }else if indexPath.section == 1{
            let cell:SubscriptionCell = tableView.cellForRowAtIndexPath(indexPath) as SubscriptionCell
            cell.toggle()
        }else if indexPath.section == 2{
            let ad:NativeAd = adList!.get(indexPath.row)
            AloeUtil.openBrowser(ad.linkUrl)
        }
        
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if SUtil.isNoAd(){
            return 2
        }
        return 3
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        // 当アプリについて
        if section == 0{
            if SUtil.isNoAd(){
                return 3
            }
            return 4
        }
        
        // 無料アプリ
        if section == 2{
            if adList == nil{
                return 0
            }
            return (adList!.count() < 10) ? adList!.count() : 10
        }
        
        if(siteList == nil){
            return 0
        }
        
        return siteList!.count()
    }

}
