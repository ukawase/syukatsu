//
//  NewsListViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NewsListViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource {
    
    private var newsList = NewsList()
    private let tableView = UITableView(frame: SUtil.headerContentsFrame())
    private let tableHeader = DekaNewsCell()
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, TAB_HEIGHT+64, 0)
        
        tableHeader.setup()
        tableHeader.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "largeTap"))
        tableView.tableHeaderView = tableHeader
        
        self.view.addSubview(tableView)
    }
    
    func largeTap(){
        tableHeader.tap()
        let news:News = Model.sharedInstance.largeNews()!
        println("\(news.title)")
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(news.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("ニュース：" + news.title)
    }
    
    override func reload() {
        newsList = Model.sharedInstance.newsList()
        tableView.reloadData()
        
        if let largeNews = Model.sharedInstance.largeNews(){
            tableHeader.reload(largeNews)
            tableView.tableHeaderView = tableHeader
        }else{
            tableView.tableHeaderView = nil
        }
    }
    
    override func getHeaderTitle() -> String {
        return "就活ニュース"
    }
    
    private func isAdIndex(indexPath:NSIndexPath)->Bool{
        if !AloeUtil.isJaDevice(){
            return false
        }
        if SUtil.isNoAd(){
            return false
        }
        return indexPath.row % NATIVE_AD_LIST_RATE == 0 && indexPath.row != 0
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if isAdIndex(indexPath){
            return 104
        }
        return 84
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return newsList.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // ad
        if self.isAdIndex(indexPath){
            let adCellIdentifier:String = "adCellIdentifier"
            var cell:AdCell? = tableView.dequeueReusableCellWithIdentifier(adCellIdentifier) as AdCell?
            
            if cell == nil{
                cell = AdCell(style:UITableViewCellStyle.Default, reuseIdentifier:adCellIdentifier)
                cell!.setup()
            }
            
            cell!.reload()
            
            return cell!
        }
        
        
        // 通常
        let cellIdentifier:String = "newsCellIdentifier"
        
        var cell:NewsCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as NewsCell?
        if cell == nil {
            cell = NewsCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let news:News = newsList.get(indexPath.row)
        cell!.reload(news)
        
        return cell!
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if isAdIndex(indexPath){
            let cell:AdCell = tableView.cellForRowAtIndexPath(indexPath) as AdCell
            let ad:NativeAd = cell.tap()
            let vc = AppViewController()
            vc.setup(ad)
            
            self.navigationController!.pushViewController(vc, animated: true)
            return
        }
        
        let cell:NewsCell = tableView.cellForRowAtIndexPath(indexPath) as NewsCell
        cell.tap()
        
        let news:News = newsList.get(indexPath.row)
        println("\(news.title)")
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(news.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("ニュース：" + news.title)
    }

}
