//
//  NaverViewController.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NaverViewController: BaseViewController, UITableViewDelegate, UITableViewDataSource  {

    private var naverList:NaverList = NaverList()
    private let tableView = UITableView(frame: SUtil.headerContentsFrame())
    
    func setup(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = UITableViewCellSeparatorStyle.None
        tableView.contentInset = UIEdgeInsetsMake(0, 0, TAB_HEIGHT+64, 0)
        
        self.view.addSubview(tableView)
    }
    
    override func reload() {
        naverList = Model.sharedInstance.naverList()
        tableView.reloadData()
    }
    
    override func getHeaderTitle() -> String {
        return "就活まとめ"
    }
    
    private func isAdIndex(indexPath:NSIndexPath)->Bool{
        if !AloeUtil.isJaDevice(){
            return false
        }
        if SUtil.isNoAd(){
            return false
        }
        return indexPath.row % NATIVE_AD_LIST_RATE == 0 && indexPath.row != 0
    }
    
    // MARK: UITableViewDelegate, UITableViewDataSource
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if isAdIndex(indexPath){
            return 104
        }
        return 85
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return naverList.count()
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // ad
        if self.isAdIndex(indexPath){
            let adCellIdentifier:String = "adCellIdentifier"
            var cell:AdCell? = tableView.dequeueReusableCellWithIdentifier(adCellIdentifier) as AdCell?
            
            if cell == nil{
                cell = AdCell(style:UITableViewCellStyle.Default, reuseIdentifier:adCellIdentifier)
                cell!.setup()
            }
            
            cell!.reload()
            
            return cell!
        }
        
        let cellIdentifier:String = "naverCellIdentifier"
        
        var cell:NaverCell? = tableView.dequeueReusableCellWithIdentifier(cellIdentifier) as NaverCell?
        if cell == nil {
            cell = NaverCell(style:UITableViewCellStyle.Default, reuseIdentifier:cellIdentifier)
            cell!.setup()
        }
        
        let naver:Naver = naverList.get(indexPath.row)
        cell!.reload(naver)
        
        return cell!
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        
        // ad
        if isAdIndex(indexPath){
            let cell:AdCell = tableView.cellForRowAtIndexPath(indexPath) as AdCell
            let ad:NativeAd = cell.tap()
            let vc = AppViewController()
            vc.setup(ad)
            
            self.navigationController!.pushViewController(vc, animated: true)
            return
        }
        
        let cell:NaverCell = tableView.cellForRowAtIndexPath(indexPath) as NaverCell
        cell.tap()
        
        let naver:Naver = naverList.get(indexPath.row)
        println("\(naver.title)")
        
        let webView:WebContentsViewController = WebContentsViewController()
        webView.setup()
        webView.loadUrl(naver.linkUrl)
        self.navigationController!.pushViewController(webView, animated: true)
        
        SUtil.trackScreen("naver：" + naver.title)
    }

}
