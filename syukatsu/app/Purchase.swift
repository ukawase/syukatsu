//
//  Purchase.swift
//  syukatsu
//
//  Created by kawase yu on 2015/01/07.
//  Copyright (c) 2015年 aloeproject. All rights reserved.
//

import UIKit

typealias PurchaseProductInfoCallback = (SKProduct)->()



private let PurchaseD:Purchase.ProductDelegate = Purchase.ProductDelegate()

class Purchase: NSObject {
    
    // MARK: INNER CLASS START
    
    private class ProductDelegate:NSObject, SKProductsRequestDelegate{
        
        var callback:PurchaseProductInfoCallback?
        
        override init() {
            super.init()
        }
        
        func productsRequest(request: SKProductsRequest!, didReceiveResponse response: SKProductsResponse!){
            println("productsRequest------------")
            
            if response.products.count == 0{
                println("noProduct")
                return
            }
            
            println("hasProduct")
            let product:SKProduct = response.products[0] as SKProduct
            callback!(product)
        }
        
    }
    
    // MARK: INNER CLASS END
    
    
    
    class func productInfo(productId:String, callback:PurchaseProductInfoCallback){
        let productSet = NSSet(object: productId)
        let skRequest = SKProductsRequest(productIdentifiers: productSet)
        PurchaseD.callback = callback
        skRequest.delegate = PurchaseD
        skRequest.start()
    }
    
}
