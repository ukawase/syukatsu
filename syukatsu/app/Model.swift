//
//  Model.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit
import TwitterKit

typealias ModelCallback = ()->()
typealias ModelTweetListCallback = (tweetList:TweetList)->()

private let LAST_SHOWED_NEWS_KEY = "LAST_SHOWED_NEWS_KEY"
private let LAST_SHOWED_NAVER_KEY = "LAST_SHOWED_NAVER_KEY"
private let LAST_SHOWED_PICKUP_KEY = "LAST_SHOWED_PICKUP_KEY"
private let LAST_SHOWED_INTERN_KEY = "LAST_SHOWED_INTERN_KEY"
private let NO_SUBSCRIPTION_SITE_KEY = "NO_SUBSCRIPTION_SITE_KEY"

class Model: NSObject {
    
    private var _newsList:NewsList?
    private var _thinkList:ArticleList?
    private var _worryList:ArticleList?
    private var _internList:InternList?
    private var _naverList:NaverList?
    private var _largeNews:News?
    private var _siteList:SiteList?
   
    // http://qiita.com/susieyy/items/acb3bc80a1dafe64cffd
    class var sharedInstance : Model {
        struct Static {
            static let instance : Model = Model()
        }
        return Static.instance
    }
    
    private override init(){
        super.init()
    }
    
    private let ApiJsonKey = "ApiJsonKey"
    
    func staticLoad(){
        let apiJsonStr = NSUserDefaults.standardUserDefaults().objectForKey(ApiJsonKey) as String
        let dic:NSDictionary = AloeDataUtil.strToJson(apiJsonStr)
        self.createModelFromDic(dic)
    }
    
    func reloadModel(callback:ModelCallback){
        
        let c = AloeURLLoadCommand(url_:Api.url("api.json"))
        c.setCompleteBlock{ (data) -> () in
            if let dic:NSDictionary = AloeDataUtil.dataToJsonDic(data){
                self.createModelFromDic(dic)
                
                mainThread { () -> () in
                    callback()
                }
                
                let str:String = AloeDataUtil.dataToString(data)!
                let ud = NSUserDefaults.standardUserDefaults()
                ud.setObject(str, forKey: self.ApiJsonKey)
                ud.setBool(true, forKey: self.InitializedKey)
                ud.synchronize()
            }
        }
        c.execute()
    }
    
    private func createModelFromDic(dic:NSDictionary){
        var ary:NSArray = dic["intern"] as NSArray
        self._internList = InternList(nsarray: ary)
        
        ary = dic["news"] as NSArray
        self._newsList = NewsList(nsarray: ary)
        
        ary = dic["pickup"] as NSArray
        self._thinkList = ArticleList(nsarray:ary)
        
        ary = dic["worry"] as NSArray
        self._worryList = ArticleList(nsarray: ary)
        
        // site
        ary = dic["site"] as NSArray
        self._siteList = SiteList(nsarray: ary)
        let noSubscriptionList:NSArray = self.noSubscriptionSiteList()
        for index in 0..<self._siteList!.count(){
            let site:Site = self._siteList!.get(index)
            if !self.isSubscriveSite(site, noSubscriptionSiteList: noSubscriptionList){
                site.isSubscription = false
            }
        }
        
        ary = dic["naver"] as NSArray
        self._naverList = NaverList(nsarray: ary)
        
        for i in 0..<self._newsList!.count(){
            let news:News = self._newsList!.get(i)
            self.mergeSite(news)
        }
        
        // strip
        let nl:NewsList = self.newsList()
        for ( var i = nl.count()-1; i >= 0 ; i-- ) {
            let n:News = nl.get(i)
            if !self.isSubscriveSite(n.site!, noSubscriptionSiteList: noSubscriptionList){
                nl.remove(n)
            }
        }
        
        self._largeNews = self.newsList().largeNews()
        if self._largeNews != nil{
            self.newsList().remove(self._largeNews!)
        }
    }
    
    private func mergeSite(news:News){
        let sl:SiteList = self._siteList!
        for index in 0..<sl.count(){
            let site:Site = sl.get(index)
            if site.id == news.siteId{
                news.site = site
                return
            }
        }
    }
    
    func loadTweet(callback:ModelTweetListCallback){
        
        Twitter.sharedInstance().logInGuestWithCompletion { (session, error) -> Void in
            let statusesShowEndpoint = "https://api.twitter.com/1.1/search/tweets.json"
            let params = ["q": "#就活", "count":"200"]
            var clientError : NSError?
            let request = Twitter.sharedInstance().APIClient.URLRequestWithMethod(
                "GET", URL: statusesShowEndpoint, parameters: params,
                error: &clientError)
            
            if request != nil {
                Twitter.sharedInstance().APIClient.sendTwitterRequest(request) {
                    (response, data, connectionError) -> Void in
                    if (connectionError == nil) {
                        var jsonError : NSError?
                        let json : AnyObject? =
                        NSJSONSerialization.JSONObjectWithData(data,
                            options: nil,
                            error: &jsonError)
                        
                        let statuses:NSArray = json!.objectForKey("statuses") as NSArray
                        let tweetList = TweetList(nsarray: statuses)
                        callback(tweetList: tweetList)
                    }
                    else {
                        println("Error: \(connectionError)")
                    }
                }
            }
            else {
                println("Error: \(clientError)")
            }
        }
    }
    
    func setNoSubscriptionSiteList(siteList:SiteList){
        let tmp:NSMutableArray = NSMutableArray()
        for index in 0..<siteList.count(){
            let site:Site = siteList.get(index)
            if !site.isSubscription{
                tmp.addObject(NSNumber(integer: site.id))
            }
        }
        
        let ary:NSArray = NSArray(array: tmp)
        let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        ud.setObject(ary, forKey: NO_SUBSCRIPTION_SITE_KEY)
        ud.synchronize()
    }
    
    private func noSubscriptionSiteList()->NSArray{
        if let ary = NSUserDefaults.standardUserDefaults().arrayForKey(NO_SUBSCRIPTION_SITE_KEY){
            return ary
        }
        
        return NSArray()
    }
    
    func isSubscriveSite(site:Site, noSubscriptionSiteList:NSArray)->Bool{
        for index in 0..<noSubscriptionSiteList.count{
            let siteId:Int = noSubscriptionSiteList.objectAtIndex(index) as Int
            if siteId == site.id{
                return false
            }
        }
        
        return true
    }
    
    func newsList()->NewsList{ return _newsList! }
    func thinkList()->ArticleList{ return _thinkList! }
    func worryList()->ArticleList{ return _worryList! }
    func internList()->InternList{ return _internList! }
    func naverList()->NaverList{ return _naverList! }
    func largeNews()->News?{ return _largeNews }
    func siteList()->SiteList{ return _siteList! }
    
    private func currentNewsPublishedAt()->Int{
        if self.newsList().count() == 0{
            return NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_NEWS_KEY)
        }
        let listPublished:Int = self.newsList().get(0).publishedAt
        let largePublished:Int = (self.largeNews() != nil) ? self.largeNews()!.publishedAt : 0
        
        let lastShowedNews:Int =  (listPublished > largePublished) ? listPublished : largePublished
        
        return lastShowedNews
    }
    
    private func currentPickupPublishedAt()->Int{
        if self.thinkList().count() == 0 || self.worryList().count() == 0{
            return NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_PICKUP_KEY)
        }
        
        let thinkPublishedAt:Int = self.thinkList().get(0).published_at
        let worryPublishedAt:Int = self.worryList().get(0).published_at
    
        let lastShowedPickup:Int = (thinkPublishedAt > worryPublishedAt) ? thinkPublishedAt : worryPublishedAt
        return lastShowedPickup
    }
    
    func showNews(){
        let lastShowedNews = self.currentNewsPublishedAt()
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setInteger(lastShowedNews, forKey: LAST_SHOWED_NEWS_KEY)
        ud.synchronize()
    }
    
    func showPickup(){
        let lastShowedPickup = self.currentPickupPublishedAt()
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setInteger(lastShowedPickup, forKey: LAST_SHOWED_PICKUP_KEY)
        ud.synchronize()
    }
    
    func showNaver(){
        if self.naverList().count() == 0{
            return
        }
        let lastShowedNaver:Int = self.naverList().get(0).createdAt
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setInteger(lastShowedNaver, forKey: LAST_SHOWED_NAVER_KEY)
        ud.synchronize()
    }
    
    func showIntern(){
        if self.internList().count() == 0{
            return
        }
        let lastShowedIntern:Int = self.internList().get(0).published_at
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setInteger(lastShowedIntern, forKey: LAST_SHOWED_INTERN_KEY)
        ud.synchronize()
    }
    
    func hasNewNews()->Bool{
        return currentNewsPublishedAt() > NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_NEWS_KEY)
    }
    
    func hasNewPickup()->Bool{
        return currentPickupPublishedAt() > NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_PICKUP_KEY)
    }
    
    func hasNewNaver()->Bool{
        return self.naverList().get(0).createdAt > NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_NAVER_KEY)
    }
    
    func hasNewIntern()->Bool{
        return self.internList().get(0).published_at > NSUserDefaults.standardUserDefaults().integerForKey(LAST_SHOWED_INTERN_KEY)
    }
    
    private let InitializedKey = "InitializedKey"
    
    func isInitialized()->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(InitializedKey)
    }
    
}
