//
//  NewsCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NewsCell: UITableViewCell {

    private var news:News?
    private let titleLabel = UILabel(frame: CGRectMake(13.5, 12, 227, 39))
    private let iconView = UIImageView(frame: CGRectMake(242, 5, 73, 73))
    private let siteLabel = UILabel(frame: CGRectMake(13.5, 60, 227, 10))
    private let hilightView = UIView(frame: CGRectMake(0, 0, 320, 84))
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        self.clipsToBounds = false
        self.contentView.clipsToBounds = false
        
        hilightView.backgroundColor = UIColorFromHex(0xefefef)
        hilightView.alpha = 0
        
        iconView.contentMode = UIViewContentMode.ScaleAspectFill
        iconView.clipsToBounds = true
        titleLabel.textColor = UIColorFromHex(0x333333)
        titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
        titleLabel.numberOfLines = 2
        
        siteLabel.font = UIFont.systemFontOfSize(9.0)
        siteLabel.layer.cornerRadius = 2
        siteLabel.layer.borderWidth = 0.5
        siteLabel.clipsToBounds = true
        siteLabel.textAlignment = NSTextAlignment.Center
        
        let line:UIView = UIView(frame: CGRectMake(0, 83, 320, 1))
        line.backgroundColor = UIColorFromHex(0xcccccc)
        
        self.contentView.addSubview(hilightView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(iconView)
        self.contentView.addSubview(siteLabel)
        self.contentView.addSubview(line)
    }
    
    func reload(news:News){
        self.news = news
        
        titleLabel.text = news.title
        
        siteLabel.frame = CGRectMake(13.5, 60, 227, 10)
        siteLabel.text = news.site!.name
        siteLabel.sizeToFit()
        siteLabel.frame.size.width += 10
        siteLabel.frame.size.height += 4
        siteLabel.textColor = news.site!.color
        siteLabel.layer.borderColor = news.site!.color.CGColor
        
        iconView.image = nil
        iconView.alpha = 0
        
        titleLabel.frame = news.hasImage() ? CGRectMake(13.5, 12, 227, 39) : CGRectMake(13.5, 12, 301.5, 39)
        
        if !news.hasImage(){
            return
        }
        
        AloeImageCache.loadImage(news.imageUrl, callback: { (image, url, useCache) -> () in
            if self.news!.imageUrl != url{
                return
            }
            
            self.iconView.image = image
            if useCache{
                self.iconView.alpha = 1.0
                return
            }
            AloeTween.doTween(0.2, ease: AloeEase.OutCirc, progress: { (val) -> () in
                self.iconView.alpha = val
            })
        }) { (error) -> () in
            self.titleLabel.frame = CGRectMake(13.5, 12, 301.5, 39)
        }
    }
    
    func tap(){
        AloeTweenChain().add(0.1, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.hilightView.alpha = val
        }).wait(0.5).call({ () -> () in
            self.hilightView.alpha = 0
        }).execute()
    }

}
