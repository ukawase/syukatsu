//
//  SubscriptionCell.swift
//  nich
//
//  Created by kawase yu on 2014/11/15.
//  Copyright (c) 2014年 marimo. All rights reserved.
//

import UIKit

class SubscriptionCell: UITableViewCell {
    
    private var site:Site?;
    private let label:UILabel = UILabel(frame: CGRectMake(10, 5, AloeDeviceUtil.windowWidth()-100, 34))
    private let switchView:UISwitch = UISwitch()
    
    func setup(){
        label.font = UIFont.systemFontOfSize(14.0)
        label.textColor = UIColorFromHex(0x252525)
        
        let view:UIView = self.contentView
        view.addSubview(label)
        
        switchView.frame.origin.x = AloeDeviceUtil.windowWidth() - switchView.frame.size.width - 10
        switchView.frame.origin.y = (44 - switchView.frame.size.height) / 2
        switchView.userInteractionEnabled = false
        view.addSubview(switchView)
    }
    
    func reload(site:Site){
        self.site = site;
        
        label.text = site.name
        switchView.setOn(site.isSubscription, animated: false)
    }
    
    func toggle(){
        switchView.setOn(!switchView.on, animated: true)
        site!.isSubscription = switchView.on
    }

}
