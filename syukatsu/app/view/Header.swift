//
//  Header.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Header: NSObject {
   
    private let view = UIView(frame: CGRectMake(0, 0, 320, 64))
    private let bgView = UIView(frame: CGRectMake(0, 0, 320, 64))
    private let titleLabel = UILabel(frame: CGRectMake(44, 20, 320-88, 44))
//    private let favoritButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
    
    func setup(){
        
        titleLabel.textAlignment = NSTextAlignment.Center
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.systemFontOfSize(20)
        
        bgView.backgroundColor = UIColorFromHex(0x3b907f)
//        bgView.backgroundColor = UIColorFromHex(0x000000)
        view.addSubview(bgView)
        
//        favoritButton.setImage(UIImage(named: "favoritButtonOff"), forState: UIControlState.Normal)
//        favoritButton.setImage(UIImage(named: "favoritButtonOn"), forState: UIControlState.Selected)
//        favoritButton.addTarget(self, action: "tapFavorit:", forControlEvents: UIControlEvents.TouchUpInside)
//        favoritButton.frame = CGRectMake(320-44, 20, 44, 44)
//        view.addSubview(favoritButton)
        
        let line = UIView(frame: CGRectMake(0, 63.5, 320, 0.5))
        line.backgroundColor = UIColorFromHex(0x999999)
        view.addSubview(line)
        
        view.addSubview(titleLabel)
    }
    
    func getView()->UIView{
        return view
    }
    
    func setTitle(title:String){
        titleLabel.text = title
    }
    
    func tapFavorit(button:UIButton){
        button.selected = !button.selected
    }
    
    func hide(){
        println("hide")
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc) { (val) -> () in
            self.view.alpha = 1.0 - val
        }
    }
    
    func show(){
        AloeTween.doTween(0.2, ease: AloeEase.InCirc) { (val) -> () in
            self.view.alpha = val
        }
    }
    
    
}
