//
//  NativeAdView.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/08.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation


private let NATIVE_AD_VIEW_HEIGHT:CGFloat = 90

protocol NativeAdViewDelegate{
    
    func onTapNativeAd(nativeAd:NativeAd)
    
}

class NativeAdView:NSObject{
    
    let view:UIView = UIView(frame: CGRectMake(0, -NATIVE_AD_VIEW_HEIGHT, AloeDeviceUtil.windowHeight(), NATIVE_AD_VIEW_HEIGHT))
    let iconView:UIImageView = UIImageView(frame: CGRectMake(10, 10, 40, 40))
    let titleLabel:UILabel = UILabel(frame: CGRectMake(60, 10, AloeDeviceUtil.windowWidth()-60-10-15, 15))
    let timeLabel:UILabel = UILabel(frame:CGRectMake(AloeDeviceUtil.windowWidth()-10-8, 8, 15, 15))
    let contentLabel:UILabel = UILabel(frame: CGRectMake(60, 28, AloeDeviceUtil.windowWidth()-60-10, 45))
    var nativeAd:NativeAd?
    var timer:NSTimer?
    var current:Int = 10
    var delegate:NativeAdViewDelegate?
    
    override init(){
        let bgView:UIView = UIView(frame: CGRectMake(0, -115, AloeDeviceUtil.windowWidth(), 200))
        bgView.backgroundColor = UIColor.blackColor()
        bgView.alpha = 0.7
        view.addSubview(bgView)
        
        titleLabel.font = UIFont.boldSystemFontOfSize(13.0)
        titleLabel.textColor = UIColor.whiteColor()
        contentLabel.font = UIFont.systemFontOfSize(12.0)
        contentLabel.textColor = UIColor.whiteColor()
        contentLabel.numberOfLines = 0
        
        timeLabel.font = UIFont.boldSystemFontOfSize(12.0)
        timeLabel.textColor = UIColor.whiteColor()
        timeLabel.textAlignment = NSTextAlignment.Right
        
        iconView.layer.cornerRadius = 7
        iconView.clipsToBounds = true
        
        super.init()
        
        let closeButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        closeButton.setImage(UIImage(named: "nativeAdClose"), forState: UIControlState.Normal)
        closeButton.frame = CGRectMake(8, 47, 44, 44)
        closeButton.addTarget(self, action: "tapClose", forControlEvents: UIControlEvents.TouchUpInside)
        
        view.addSubview(iconView)
        view.addSubview(titleLabel)
        view.addSubview(timeLabel)
        view.addSubview(contentLabel)
        view.addSubview(closeButton)
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap"))
    }
    
    func tap(){
//        AloeUtil.openBrowser(nativeAd!.linkUrl)
        delegate!.onTapNativeAd(nativeAd!)
        
        SUtil.trackEvent("smallNativeAd", action:"showDetail", label:nativeAd!.title)
    }
    
    func tapClose(){
        self.hide()
        println("tapClose")
    }
    
    func startTimer(){
        self.stopTimer()
        timeLabel.text = String(current)
        timer = NSTimer(timeInterval: 1.0, target: self, selector: "updateTimelabel", userInfo: nil, repeats: true)
        NSRunLoop.currentRunLoop().addTimer(timer!, forMode: NSRunLoopCommonModes)
    }
    
    @objc func updateTimelabel(){
        current--;
        timeLabel.text = String(current)
        if(current == 0){
            self.hide()
        }
    }
    
    func stopTimer(){
        current = 10
        if(timer != nil){
            timer!.invalidate()
            timer = nil
        }
    }
    
    func tryShow(){
        if SUtil.isNoAd(){
            return
        }
        if(!AloeUtil.isJaDevice()){
            return
        }
        var ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var num:Int = ud.integerForKey(tryNativeAdNumKey)
        num++
        ud.setInteger(num, forKey: tryNativeAdNumKey)
        ud.synchronize()
        if num % NATIVE_AD_SHOW_RATE == 0 {
            self.show()
            SUtil.trackEvent("smallNativeAd", action:"show", label:nativeAd!.title)
        }
    }
    
    private func show(){
//        if(!Ad.sharedInstance.isReady()){
//            return
//        }
//        if timer != nil && timer!.valid{
//            return
//        }
        self.startTimer()
        self.reload()
        AloeTween.doTween(0.3, ease: AloeEase.OutBack) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, (NATIVE_AD_VIEW_HEIGHT + 64 ) * val)
        }
    }
    
    func hide(){
        self.stopTimer()
        if(view.transform.ty != (NATIVE_AD_VIEW_HEIGHT + 64 )){
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.InBack) { (val) -> () in
            let translation:CGAffineTransform = CGAffineTransformMakeTranslation(0, (NATIVE_AD_VIEW_HEIGHT + 64 )-((NATIVE_AD_VIEW_HEIGHT + 64 )*val))
            self.view.transform = translation
        }
    }
    
    func reload(){
        nativeAd = Ad.sharedInstance.getNativeAd()
        titleLabel.text = "【無料】" + nativeAd!.title
        contentLabel.text = nativeAd!.content
        
        iconView.image = nil
        AloeImageCache.loadImage(nativeAd!.iconUrl, callback: { (image, url, useCache) -> () in
            if(self.nativeAd!.iconUrl != url){
                return
            }
            self.iconView.image = image
        }) { (error) -> () in
            
        }
    }
    
    func getView()->UIView{
        return view
    }
}