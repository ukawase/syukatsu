//
//  AdCell.swift
//  syukatsu
//
//  Created by kawase yu on 2015/01/05.
//  Copyright (c) 2015年 aloeproject. All rights reserved.
//

import UIKit

class AdCell: UITableViewCell {

    // MARK: INNER CLASS START
    
    class AdCellCard:UIView{
        
        internal var ad:NativeAd?
        internal let wrapView = UIView(frame: CGRectMake(0, 0, 320, 104))
        private let titleLabel = UILabel(frame: CGRectMake(100, 12, 205, 20))
        private let contentLabel = UILabel(frame: CGRectMake(100, 35, 205, 60))
        private let icon:UIImageView = UIImageView(frame: CGRectMake(12, 12, 80, 80))
        private let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.White)
        
        func setup(){
            self.frame = CGRectMake(0, 0, 320, 104)
            
            titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
            titleLabel.textColor = UIColorFromHex(0xffffff)
            
            contentLabel.font = UIFont.systemFontOfSize(14.0)
            contentLabel.textColor = UIColorFromHex(0xffffff)
            contentLabel.numberOfLines = 3
            
            indicator.frame = icon.frame
            
            icon.contentMode = UIViewContentMode.ScaleAspectFit
            icon.layer.cornerRadius = 18
            icon.clipsToBounds = true
            
            wrapView.addSubview(titleLabel)
            wrapView.addSubview(contentLabel)
            wrapView.addSubview(indicator)
            wrapView.addSubview(icon)
            wrapView.backgroundColor = UIColor.clearColor()
            self.addSubview(wrapView)
            
            wrapView.transform = CGAffineTransformMakeScale(0.8, 0.8)
            self.alpha = 0.2
            
            // event
            let rightSwipe = UISwipeGestureRecognizer(target: self, action: "rightSwipe")
            rightSwipe.direction = UISwipeGestureRecognizerDirection.Right
            self.addGestureRecognizer(rightSwipe)
            
            let upSwipe = UISwipeGestureRecognizer(target: self, action: "rightSwipe")
            upSwipe.direction = UISwipeGestureRecognizerDirection.Up
            self.addGestureRecognizer(upSwipe)
            
            let leftSwipe = UISwipeGestureRecognizer(target: self, action: "leftSwipe")
            leftSwipe.direction = UISwipeGestureRecognizerDirection.Left
            self.addGestureRecognizer(leftSwipe)
            
            let downSwipe = UISwipeGestureRecognizer(target: self, action: "leftSwipe")
            downSwipe.direction = UISwipeGestureRecognizerDirection.Down
            self.addGestureRecognizer(downSwipe)
        }
        
        func rightSwipe(){
            let nextView:AdCellCard = self.superview!.subviews[self.superview!.subviews.count-2] as AdCellCard
            nextView.show()
            AloeTweenChain().add(0.3, ease: AloeEase.OutCirc, progress:{ (val) -> () in
                let toX:CGFloat = 320 * val
                let t = CGAffineTransformMakeTranslation(toX, 0)
                self.transform = t
            }).call({ () -> () in
                self.superview!.sendSubviewToBack(self)
                self.transform = CGAffineTransformMakeTranslation(0, 0)
                self.wrapView.transform = CGAffineTransformMakeScale(0.8, 0.8)
                self.alpha = 0
            }).execute()
            
//            delegate!.onHideBannerAd(self.tag)
        }
        
        func leftSwipe(){
            let nextView:AdCellCard = self.superview!.subviews[self.superview!.subviews.count-2] as AdCellCard
            nextView.show()
            AloeTweenChain().add(0.3, ease: AloeEase.OutCirc, progress:{ (val) -> () in
                let toX:CGFloat = -320 * val
                let t = CGAffineTransformMakeTranslation(toX, 0)
                self.transform = t
            }).call({ () -> () in
                self.superview!.sendSubviewToBack(self)
                self.transform = CGAffineTransformMakeTranslation(0, 0)
                self.wrapView.transform = CGAffineTransformMakeScale(0.8, 0.8)
                self.alpha = 0
            }).execute()
            
//            delegate!.onHideBannerAd(self.tag)
        }
        
        func show(){
            AloeTween.doTween(0.2, ease: AloeEase.OutBack) { (val) -> () in
                self.wrapView.transform = CGAffineTransformMakeScale(0.8 + (0.2*val), 0.8 + (0.2*val))
                self.alpha = 0.2 + (0.8*val)
            }
        }
        
        func reload(ad:NativeAd){
            self.ad = ad
            
            titleLabel.text = ad.title
            contentLabel.text = ad.content
            icon.image = nil
            indicator.startAnimating()
            
            self.backgroundColor = SUtil.randColor()
            
            AloeImageCache.loadImage(ad.iconUrl, callback: { (image, url, useCache) -> () in
                if self.ad!.iconUrl != url{
                    return
                }
                self.indicator.stopAnimating()
                self.icon.image = image
                }) { (error) -> () in
                    
            }
            
        }
    }
    
    // MARK: INNER CLASS END
    
    private var cardList:[AdCellCard] = []
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        self.backgroundColor = UIColor.blackColor()
        
        let line = UIView(frame: CGRectMake(0, 103, 320, 1))
        line.backgroundColor = UIColorFromHex(0xcccccc)
        self.addSubview(line)
        
        let rightArrow:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        rightArrow.setImage(UIImage(named: "cellArrow"), forState: UIControlState.Normal)
        rightArrow.addTarget(self, action: "tapRight", forControlEvents: UIControlEvents.TouchUpInside)
        rightArrow.frame = CGRectMake(320-50+15, 0, 50, 104)
        rightArrow.alpha = 0.5
        self.addSubview(rightArrow)
        
        let leftArrow:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        leftArrow.setImage(UIImage(named: "leftArrow"), forState: UIControlState.Normal)
        leftArrow.addTarget(self, action: "tapLeft", forControlEvents: UIControlEvents.TouchUpInside)
        leftArrow.frame = CGRectMake(-15, 0, 50, 104)
        leftArrow.alpha = 0.5
        self.addSubview(leftArrow)
    }
    
    func tapRight(){
        self.currentCard().rightSwipe()
    }
    
    func tapLeft(){
        self.currentCard().leftSwipe()
    }
    
    func reload(){
        
        let adList:NativeAdList = Ad.sharedInstance.getIconAdList()
        adList.shuffle()
        for index in 0..<adList.count(){
            if index == 10{
                break
            }
            let ad:NativeAd = adList.get(index)
            
            var v:AdCellCard
            if index < cardList.count{
                v = cardList[index]
            }else{
                v = AdCellCard()
                cardList.append(v)
                self.contentView.addSubview(v)
                v.setup()
            }
            
            v.reload(ad)
            v.tag = index
        }
        
        let v:AdCellCard = self.currentCard()
        v.alpha = 1.0
        v.wrapView.transform = CGAffineTransformMakeScale(1.0, 1.0)
//        v.transform = CGAffineTransformMakeTranslation(5, 0)
    }
    
    private func currentCard()->AdCellCard{
        return self.contentView.subviews[self.contentView.subviews.count-1] as AdCellCard
    }
    
    func tap()->NativeAd{
        
//        self.transform = CGAffineTransformMakeScale(0.9, 0.9)
//        AloeTween.doTween(0.2, ease: AloeEase.OutBack) { (val) -> () in
//
//        }
        
        let card:AdCellCard = self.contentView.subviews[ self.contentView.subviews.count-1] as AdCellCard
        let ad:NativeAd = card.ad!
        
        SUtil.trackEvent("infeedNativeAd", action:"showDetail", label:ad.title)
        
        return ad
    }

}
