//
//  IconAdView.swift
//  syukatsu
//
//  Created by kawase yu on 2015/01/06.
//  Copyright (c) 2015年 aloeproject. All rights reserved.
//

import UIKit

class IconAdView: NSObject, IMobileSdkAdsDelegate {

    private let view = UIView(frame:CGRectMake(0, 0, 320, 75+40))
    
    class var sharedInstance : IconAdView {
        struct Static {
            static let instance : IconAdView = IconAdView()
        }
        return Static.instance
    }
    
    private override init(){
        super.init()
        
        let bgView = UIView(frame: CGRectMake(0, 0, 320, 75+40))
        bgView.backgroundColor = UIColor.blackColor()
        bgView.alpha = 0.7
        view.addSubview(bgView)
        
        let innerView = UIView(frame: CGRectMake(0, 40, 320, 75))
        view.addSubview(innerView)
    
        if AloeUtil.isJaDevice(){
            ImobileSdkAds.registerWithPublisherID(IMOBILE_PUBLISHER_ID, mediaID:IMOBILE_MEDIA_ID, spotID:IMOBILE_SPOT_ID_ICON)
            ImobileSdkAds.startBySpotID(IMOBILE_SPOT_ID_ICON)
            let params = ImobileSdkAdsIconParams()
            params.iconNumber = 4
            ImobileSdkAds.showBySpotID(IMOBILE_SPOT_ID_ICON, view: innerView, iconPrams: params)
        }else{
            // TODO imobileの情報取れないので いったん非表示
            view.hidden = true
            innerView.frame.size.height = 50
            ImobileSdkAds.setTestMode(true)
            ImobileSdkAds.registerWithPublisherID(IMOBILE_PUBLISHER_ID, mediaID:IMOBILE_MEDIA_ID, spotID:IMOBILE_SPOT_ID_BANNER)
            ImobileSdkAds.setSpotDelegate(IMOBILE_SPOT_ID_BANNER, delegate: self)
            ImobileSdkAds.startBySpotID(IMOBILE_SPOT_ID_BANNER)
            ImobileSdkAds.showBySpotID(IMOBILE_SPOT_ID_BANNER, view: innerView)
        }
        
        view.transform = CGAffineTransformMakeTranslation(0, -115)
    }
    
    func getView()->UIView{
        return view
    }
    
    func show(){
        if SUtil.isNoAd(){
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc) { (val) -> () in
            let y:CGFloat = -115 + (115 * val)
            self.view.transform = CGAffineTransformMakeTranslation(0, y)
        }
    }

    func staticShow(){
        if SUtil.isNoAd(){
            return
        }
        self.view.transform = CGAffineTransformMakeTranslation(0, 0)
    }
    
    func hide(){
        AloeTween.doTween(0.2, ease: AloeEase.OutCirc) { (val) -> () in
            let y:CGFloat = -115 * val
            self.view.transform = CGAffineTransformMakeTranslation(0, y)
        }
    }
    
    // MARK:
    func imobileSdkAdsSpot(spotId: String!, didFailWithValue value: ImobileSdkAdsFailResult) {
        println("imobileSdkAdsSpot didFailWithValue:\(value)")
    }
    
    func imobileSdkAdsSpot(spotId: String!, didReadyWithValue value: ImobileSdkAdsReadyResult) {
        println("imobileSdkAdsSpot didReadyWithValue:\(value)")
    }
    
    func imobileSdkAdsSpotIsNotReady(spotId: String!) {
        println("imobileSdkAdsSpot imobileSdkAdsSpotIsNotReady")
    }
    
}
