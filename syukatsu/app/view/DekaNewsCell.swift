//
//  DekaNewsCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class DekaNewsCell: UIView {

    private let imageView = UIImageView(frame: CGRectMake(0, 0, 320, 138))
    private let titleLabel = UILabel(frame: CGRectMake(15, 150, 293, 42))
    private let hilightView = UIView(frame: CGRectMake(0, 0, 320, 205))
    private let indicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    private var news:News?
    
    func setup(){
        self.frame = CGRectMake(0, 0, 320, 205)
        
        hilightView.backgroundColor = UIColorFromHex(0xefefef)
        hilightView.alpha = 0
        self.addSubview(hilightView)
        
        imageView.clipsToBounds = true
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        
        indicator.frame = imageView.frame
        
        self.addSubview(indicator)
        self.addSubview(imageView)
        self.addSubview(titleLabel)
        
        titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
        titleLabel.textColor = UIColorFromHex(0x333333)
        titleLabel.numberOfLines = 2
        
        let line = UIView(frame:CGRectMake(0, 204, 320, 1))
        line.backgroundColor = UIColorFromHex(0xcccccc)
        self.addSubview(line)
    }
    
    func tap(){
        AloeTweenChain().add(0.1, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.hilightView.alpha = val
        }).wait(0.5).call({ () -> () in
            self.hilightView.alpha = 0
        }).execute()
    }
    
    func reload(news:News){
        self.news = news
        
        titleLabel.text = news.title
        imageView.image = nil
        self.imageView.backgroundColor = UIColor.whiteColor()
        indicator.startAnimating()
        
        AloeImageCache.loadImage(news.imageUrl, callback: { (image, url, useCache) -> () in
            if self.news!.imageUrl != url{
                return
            }
            self.imageView.image = image
            self.indicator.stopAnimating()
        }) { (error) -> () in
            self.imageView.backgroundColor = SUtil.randColor()
            self.indicator.stopAnimating()
        }
    }

}
