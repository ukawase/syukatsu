//
//  NaverCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NaverCell: UITableViewCell {

    private var naver:Naver?
    // 85
    private let hilightView = UIView(frame: CGRectMake(0, 0, 320, 85))
    private let iconImageView = UIImageView(frame: CGRectMake(15, 15, 55, 55))
    private let titleLabel = UILabel(frame: CGRectMake(85, 15, 213, 38))
    private let viewLabel = UILabel(frame: CGRectMake(85, 60, 213, 15))
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        let v:UIView = self.contentView
        
        hilightView.backgroundColor = UIColorFromHex(0xefefef)
        hilightView.alpha = 0
        v.addSubview(hilightView)
        
        iconImageView.clipsToBounds = true
        iconImageView.contentMode = UIViewContentMode.ScaleAspectFill
        v.addSubview(iconImageView)
        
        titleLabel.textColor = UIColorFromHex(0x333333)
        titleLabel.font = UIFont.boldSystemFontOfSize(15.0)
        titleLabel.numberOfLines = 2
        v.addSubview(titleLabel)
        
        viewLabel.textColor = UIColorFromHex(0x666666)
        viewLabel.font = UIFont.systemFontOfSize(10.0)
        v.addSubview(viewLabel)
        
        let line = UIView(frame: CGRectMake(0, 84, 320, 1))
        line.backgroundColor = UIColorFromHex(0xcccccc)
        v.addSubview(line)
    }
    
    func reload(naver:Naver){
        self.naver = naver
        
        titleLabel.text = naver.title
        viewLabel.text = String(naver.views) + " views"
        
        iconImageView.image = nil
        iconImageView.alpha = 0
        
        titleLabel.frame = CGRectMake(85, 15, 213, 38)
        viewLabel.frame = CGRectMake(85, 60, 213, 15)
        
        if !naver.hasImage(){
            titleLabel.frame = CGRectMake(15, 15, 283, 38)
            viewLabel.frame = CGRectMake(15, 60, 283, 15)
            return
        }
        
        AloeImageCache.loadImage(naver.imageUrl, callback: { (image, url, useCache) -> () in
            if self.naver!.imageUrl != url{
                return
            }
            
            self.iconImageView.image = image
            
            if useCache{
                self.iconImageView.alpha = 1.0
                return
            }
            
            AloeTween.doTween(0.2, ease: AloeEase.OutCirc, progress: { (val) -> () in
                self.iconImageView.alpha = val
            })
            
        }) { (error) -> () in
            
        }
    }
    
    func tap(){
        AloeTweenChain().add(0.1, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.hilightView.alpha = val
        }).wait(0.5).call({ () -> () in
            self.hilightView.alpha = 0
        }).execute()
    }

}
