//
//  TweetCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/26.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

private var tweetCellContentLabel:UILabel?

class TweetCell: UITableViewCell {

    private var tweet:TweetModel?
    private let iconImageView = UIImageView(frame: CGRectMake(12, 12, 49, 49))
    private let nameLabel = UILabel(frame: CGRectMake(67, 12, 195, 15))
    private let screenNameLabel = UILabel(frame: CGRectMake(67, 30, 244, 14))
    private let timeLabel = UILabel(frame: CGRectMake(308-100, 12, 100, 15))
    private let contentLabel = UILabel(frame: CGRectMake(67, 47, 244, 158))
    private let hilightView = UIView(frame: CGRectMake(0, 0, 320, 211))
    private let line = UIView()
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        iconImageView.contentMode = UIViewContentMode.ScaleAspectFit
        iconImageView.clipsToBounds = true
        iconImageView.layer.cornerRadius = 5
        
        nameLabel.textColor = UIColorFromHex(0x333333)
        nameLabel.font = UIFont.boldSystemFontOfSize(14)
        
        screenNameLabel.textColor = UIColorFromHex(0x666666)
        screenNameLabel.font = UIFont.systemFontOfSize(13.0)
        
        timeLabel.textColor = UIColorFromHex(0x666666)
        timeLabel.font = UIFont.systemFontOfSize(13.0)
        timeLabel.textAlignment = NSTextAlignment.Right
        
        contentLabel.textColor = UIColorFromHex(0x333333)
        contentLabel.font = UIFont.systemFontOfSize(14)
        contentLabel.numberOfLines = 9
        
        hilightView.backgroundColor = UIColorFromHex(0xefefef)
        hilightView.alpha = 0
        
        let v:UIView = self.contentView
        v.addSubview(hilightView)
        v.addSubview(iconImageView)
        v.addSubview(nameLabel)
        v.addSubview(screenNameLabel)
        v.addSubview(timeLabel)
        v.addSubview(contentLabel)
        
        line.backgroundColor = UIColorFromHex(0xcccccc)
        v.addSubview(line)
    }
    
    private func agoText()->String{
        let sec = Int(NSDate().timeIntervalSinceDate(tweet!.createdDate))
        
        if sec < 60{
            return String(sec) + "秒前"
        }else if sec < 60*60{
            return String(sec / 60) + "分前"
        }else if sec < 60*60*24{
            return String(sec / 60 / 60) + "時間前"
        }else {
            return String(sec / 60 / 60 / 24) + "日前"
        }
    }
    
    func reload(tweet:TweetModel){
        self.tweet = tweet
        
        nameLabel.text = tweet.name
        screenNameLabel.text = "@" + tweet.screenName
        timeLabel.text = self.agoText()
        
        contentLabel.frame = CGRectMake(67, 47, 244, 158)
        contentLabel.text = tweet.txt
        contentLabel.sizeToFit()
        
        line.frame = CGRectMake(0, TweetCell.height(tweet.txt)-1, 320, 1)
        hilightView.frame.size.height = TweetCell.height(tweet.txt)
        
        iconImageView.image = nil
        AloeImageCache.loadImage(tweet.iconUrl, callback: { (image, url, useCache) -> () in
            if url != self.tweet!.iconUrl{
                return
            }
            
            self.iconImageView.image = image
            
        }) { (error) -> () in
            
        }
    }
    
    func tap(){
        AloeTweenChain().add(0.1, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.hilightView.alpha = val
        }).wait(0.5).call({ () -> () in
            self.hilightView.alpha = 0
        }).execute()
    }
    
    class func height(str:String)->CGFloat{
        if tweetCellContentLabel == nil{
            tweetCellContentLabel = UILabel()
            tweetCellContentLabel!.font = UIFont.systemFontOfSize(14)
            tweetCellContentLabel!.numberOfLines = 9
        }
        
        tweetCellContentLabel!.frame = CGRectMake(67, 47, 244, 158)
        tweetCellContentLabel!.text = str
        tweetCellContentLabel!.sizeToFit()
        return 47 + tweetCellContentLabel!.frame.size.height + 12
    }

}
