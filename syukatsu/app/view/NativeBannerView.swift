//
//  NativeBannerView.swift
//  syukatsu
//
//  Created by kawase yu on 2015/01/01.
//  Copyright (c) 2015年 aloeproject. All rights reserved.
//

import UIKit

private let NativeBannerViewHeight:CGFloat = 220
private let NativeBannerViewShowY:CGFloat = NativeBannerViewHeight + 64

protocol NativeBannerDelegate{
    func onSelectNativeAd(nativeAd:NativeAd)
    func onHideBannerAd(index:Int)
}

class NativeBannerView: NSObject, NativeBannerDelegate {
    
    // MARK: INNER start
    class NativeBanner:UIView{
        
        private var nativeAd:NativeAd?
        var delegate:NativeBannerDelegate?
        
        func setup(nativeAd:NativeAd){
            self.nativeAd = nativeAd
            self.frame = CGRectMake(0, 0, 320, NativeBannerViewHeight)
            
            let bannerWidth:CGFloat = 300
            let bannerRate:CGFloat = bannerWidth / 780
            let bannerHeight = 400 * bannerRate
            
            let imageView = UIImageView()
            imageView.frame = CGRectMake(10, 10, bannerWidth, bannerHeight)
            self.addSubview(imageView)
            self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: "tap"))
            
            let rightSwipe = UISwipeGestureRecognizer(target: self, action: "rightSwipe")
            rightSwipe.direction = UISwipeGestureRecognizerDirection.Right
            self.addGestureRecognizer(rightSwipe)
            
            let upSwipe = UISwipeGestureRecognizer(target: self, action: "rightSwipe")
            upSwipe.direction = UISwipeGestureRecognizerDirection.Up
            self.addGestureRecognizer(upSwipe)
            
            let leftSwipe = UISwipeGestureRecognizer(target: self, action: "leftSwipe")
            leftSwipe.direction = UISwipeGestureRecognizerDirection.Left
            self.addGestureRecognizer(leftSwipe)
            
            let downSwipe = UISwipeGestureRecognizer(target: self, action: "leftSwipe")
            downSwipe.direction = UISwipeGestureRecognizerDirection.Down
            self.addGestureRecognizer(downSwipe)
            
            AloeImageCache.loadImage(nativeAd.bannerUrl!, callback: { (image, url, useCache) -> () in
                if self.nativeAd!.bannerUrl != url{
                    return
                }
                
                imageView.image = image
            }) { (error) -> () in
                
            }
        }
        
        func tap(){
            AloeUtil.pain(self)
            
            if !AloeUtil.isJaDevice(){
                AloeUtil.openBrowser(nativeAd!.linkUrl)
                return
            }
            
            SUtil.trackEvent("largeNativeAd", action:"showDetail", label:nativeAd!.title)
            delegate!.onSelectNativeAd(nativeAd!)
        }
        
        func rightSwipe(){
            AloeTweenChain().add(0.15, ease: AloeEase.OutCirc, progress:{ (val) -> () in
                let toAngle:CGFloat = 45 * val
                let toX:CGFloat = 200 * val
                let toY:CGFloat = -200 * val
                let radi:CGFloat = toAngle / 180 * CGFloat(M_PI)
                let t = CGAffineTransformTranslate(CGAffineTransformMakeRotation(radi), toX, toY)
                self.transform = t
            }).call({ () -> () in
                self.superview!.sendSubviewToBack(self)
            }).add(0.15, ease: AloeEase.InCirc, progress:{ (val) -> () in
                let toAngle:CGFloat = 45 - (45 * val)
                let toX:CGFloat = 200 - (200 * val)
                let toY:CGFloat = -200 + (200 * val)
                let radi:CGFloat = toAngle / 180 * CGFloat(M_PI)
                let t = CGAffineTransformTranslate(CGAffineTransformMakeRotation(radi), toX, toY)
                self.transform = t
            }).execute()
            
            delegate!.onHideBannerAd(self.tag)
        }
        
        func leftSwipe(){
            AloeTweenChain().add(0.15, ease: AloeEase.OutCirc, progress:{ (val) -> () in
                let toAngle:CGFloat = -45 * val
                let toX:CGFloat = -200 * val
                let toY:CGFloat = -200 * val
                let radi:CGFloat = toAngle / 180 * CGFloat(M_PI)
                let t = CGAffineTransformTranslate(CGAffineTransformMakeRotation(radi), toX, toY)
                self.transform = t
            }).call({ () -> () in
                self.superview!.sendSubviewToBack(self)
            }).add(0.15, ease: AloeEase.InCirc, progress:{ (val) -> () in
                let toAngle:CGFloat = -45 + (45 * val)
                let toX:CGFloat = -200 + (200 * val)
                let toY:CGFloat = -200 + (200 * val)
                let radi:CGFloat = toAngle / 180 * CGFloat(M_PI)
                let t = CGAffineTransformTranslate(CGAffineTransformMakeRotation(radi), toX, toY)
                self.transform = t
            }).execute()
            
            delegate!.onHideBannerAd(self.tag)
        }
        
    }
    
    // MARK: INNER end
    
    var delegate:NativeBannerDelegate?
    var list:NativeAdList?
    let view = UIView(frame: CGRectMake(0, -NativeBannerViewHeight, 320, NativeBannerViewHeight))
    let titleLabel = UILabel(frame: CGRectMake(52, NativeBannerViewHeight-44, 320-52-8, 44))
    private let wrapView = UIView(frame:CGRectMake(0, 0, 320, NativeBannerViewHeight))
    
    class var sharedInstance : NativeBannerView {
        struct Static {
            static let instance : NativeBannerView = NativeBannerView()
        }
        return Static.instance
    }
    
    private override init(){
        super.init()
        self.setup()
        AloeEventUtil.addGlobalEventListener(self, selector: "reload", name: ON_NATIVE_BANNER_AD_LOADED)
    }
    
    private func setup(){
        let bgView = UIView(frame: CGRectMake(0, -100, 320, NativeBannerViewHeight+100))
        bgView.backgroundColor = UIColor.blackColor()
        bgView.alpha = 0.7
        view.addSubview(bgView)
        view.addSubview(wrapView)
        
        let closeButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        closeButton.setImage(UIImage(named: "nativeAdClose"), forState: UIControlState.Normal)
        closeButton.frame = CGRectMake(8, NativeBannerViewHeight-44, 44, 44)
        closeButton.addTarget(self, action: "tapClose", forControlEvents: UIControlEvents.TouchUpInside)
        view.addSubview(closeButton)
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont.systemFontOfSize(16.0)
        view.addSubview(titleLabel)
    }
    
    func tapClose(){
        self.hide()
    }
    
    func onSelectNativeAd(nativeAd: NativeAd) {
        delegate!.onSelectNativeAd(nativeAd)
    }
    
    func onHideBannerAd(index:Int){
        var nextIndex = index-1
        if nextIndex == -1{
            nextIndex = list!.count() - 1
        }
        
        let ad:NativeAd = list!.get(nextIndex)
        titleLabel.text = ad.title
    }
    
    func getView()->UIView{
        return view
    }
    
    func reload(){
        println("reloadNativeBannerView")
        
        list = Ad.sharedInstance.getNativeAdList()
        if list!.count() == 0{
            return
        }
        
        list!.shuffle()
        for v in wrapView.subviews{
            v.removeFromSuperview()
        }
        
        var x:CGFloat = 0
        var y:CGFloat = 0
        let sa:CGFloat = 10.0 / CGFloat(list!.count())
        for index in 0..<list!.count(){
            let ad:NativeAd = list!.get(index)
            let v = NativeBanner()
            v.setup(ad)
            v.tag = index
            v.frame.origin.y = y
            v.delegate = self
            wrapView.addSubview(v)
            y += sa
            
            if index == list!.count()-1{
                titleLabel.text = ad.title
                v.transform = CGAffineTransformTranslate(CGAffineTransformMakeRotation(3 / 180 * CGFloat(M_PI)), 0, 0)
            }
        }
    }
    
    func tryShow(){
        if SUtil.isNoAd(){
            return
        }
        
        if list == nil{
            return
        }
        if list!.count() == 0{
            return
        }
        if view.transform.ty != 0{
            return
        }
        
        var ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        var num:Int = ud.integerForKey(tryNativeAdNumKey)
        num++
        ud.setInteger(num, forKey: tryNativeAdNumKey)
        ud.synchronize()
        if num % NATIVE_AD_SHOW_RATE != 0 {
            return
        }
        
        SUtil.trackEvent("largeNativeAd", action:"show", label:"")
        AloeTween.doTween(0.2, ease: AloeEase.OutBack) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, NativeBannerViewShowY*val)
        }
    }
    
    func hide(){
        if view.transform.ty != NativeBannerViewShowY{
            return
        }
        AloeTween.doTween(0.2, ease: AloeEase.InBack) { (val) -> () in
            self.view.transform = CGAffineTransformMakeTranslation(0, NativeBannerViewShowY - (NativeBannerViewShowY*val))
        }
        
        AloeThreadUtil.wait(0.3, block: { () -> () in
            self.reload()
        })
        
    }
    
    func staticHide(){
        self.view.transform = CGAffineTransformMakeTranslation(0, 0)
    }
    
}
