//
//  Tab.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

protocol TabDelegate{
    
    func onSelect(index:Int)
    
}

class Tab: NSObject {
    
    let view = UIView(frame: CGRectMake(0, AloeDeviceUtil.windowHeight()-TAB_HEIGHT, 320, TAB_HEIGHT))
    let buttonView:UIView = UIView(frame: CGRectMake(0, 0, 320, TAB_HEIGHT))
    var delegate:TabDelegate?
    private var tabList:[UIButton] = []
    private var badgeList:[UIImageView] = []
    
    func setup(){
        view.addSubview(buttonView)
        
        for index in 0..<5{
            let button:UIButton = self.createButton(index)
            tabList.append(button)
            buttonView.addSubview(button)
        }
        
        let line = UIView(frame: CGRectMake(0, 0, 320, 0.5))
        line.backgroundColor = UIColorFromHex(0x999999)
        view.addSubview(line)
    }
    
    private func createButton(index:Int)->UIButton{
        let frame:CGRect = CGRectMake(CGFloat(index)*64, 0, 64, TAB_HEIGHT)
        let button:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        button.frame = frame
        button.setImage(UIImage(named: "tab" + String(index+1) + "Off"), forState: UIControlState.Normal)
        button.setImage(UIImage(named: "tab" + String(index+1) + "On"), forState: UIControlState.Selected)
        button.addTarget(self, action: "tapButton:", forControlEvents: UIControlEvents.TouchUpInside)
        button.tag = index
        
        let badge = UIImageView(frame: CGRectMake(44.5, 3, 18, 18))
        badge.userInteractionEnabled = false
        badge.image = UIImage(named: "newBadge")
        badge.transform = CGAffineTransformMakeScale(0, 0)
        badge.alpha = 0
        badgeList.append(badge)
        button.addSubview(badge)
        
        button.selected = index == 0
        return button
    }
    
    func tapButton(button:UIButton){
                
        let index:Int = button.tag
        delegate!.onSelect(index)
        self.hideBadge(index)
        
        for i in 0..<tabList.count{
            var button:UIButton = tabList[i]
            button.selected = i == index
        }
        
        let model = Model.sharedInstance
        if index == 0{
            model.showNews()
        }else if index == 1{
            model.showPickup()
        }else if index == 2{
            model.showNaver()
        }else if index == 4{
            model.showIntern()
        }
    }
    
    func reload(){
        let model:Model = Model.sharedInstance
        
        if model.hasNewNews(){
            showBadge(0)
        }
        
        if model.hasNewPickup(){
            showBadge(1)
        }
        
        if model.hasNewNaver(){
            showBadge(2)
        }
        
        if model.hasNewIntern(){
            showBadge(4)
        }
    }
    
    private func showBadge(index:Int){
        let badge = badgeList[index]
        
        if badge.alpha == 1{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.OutBack, progress: { (val) -> () in
            badge.transform = CGAffineTransformMakeScale(val, val)
            badge.alpha = val
        })
    }
    
    private func hideBadge(index:Int){
        let badge = badgeList[index]
        
        if badge.alpha == 0{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.InCirc, progress: { (val) -> () in
            let scale:CGFloat = 1.0 - val
            badge.alpha = scale
            badge.transform = CGAffineTransformMakeScale(scale, scale)
        })
    }
    
    func getView()->UIView{
        return view
    }
    
    func show(){
        if view.transform.ty != TAB_HEIGHT{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.None) { (val) -> () in
            let y:CGFloat = TAB_HEIGHT - (TAB_HEIGHT * val)
            self.view.transform = CGAffineTransformMakeTranslation(0, y)
            self.view.userInteractionEnabled = true
        }
        
        GlobalMenuTy = 0
    }
    
    func hide(){
        if view.transform.ty != 0{
            return
        }
        
        AloeTween.doTween(0.2, ease: AloeEase.None) { (val) -> () in
            let y:CGFloat = TAB_HEIGHT * val
            self.view.transform = CGAffineTransformMakeTranslation(0, y)
            self.view.userInteractionEnabled = false
        }
        
        GlobalMenuTy = TAB_HEIGHT
    }

}
