//
//  ThinkCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class ThinkCell: UITableViewCell {

    private var article:Article?
    private let titleLabel = UILabel(frame: CGRectMake(13.5, 12, 320-27, 39))
    private let siteLabel = UILabel(frame: CGRectMake(13.5, 60, 227, 10))
    private let favoritLabel = UILabel(frame: CGRectMake(13.5, 62, 320-27, 10))
    private let hilightView = UIView(frame: CGRectMake(0, 0, 320, 84))
    
    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        hilightView.backgroundColor = UIColorFromHex(0xefefef)
        hilightView.alpha = 0
        
        titleLabel.textColor = UIColorFromHex(0x333333)
        titleLabel.font = UIFont.boldSystemFontOfSize(16.0)
        titleLabel.numberOfLines = 2
        
        siteLabel.font = UIFont.systemFontOfSize(9.0)
        siteLabel.layer.cornerRadius = 2
        siteLabel.layer.borderWidth = 0.5
        siteLabel.clipsToBounds = true
        siteLabel.textAlignment = NSTextAlignment.Center
        
        favoritLabel.textColor = UIColorFromHex(0x51a047)
        favoritLabel.font = UIFont.systemFontOfSize(11.0)
        favoritLabel.textAlignment = NSTextAlignment.Right
        
        let line:UIView = UIView(frame: CGRectMake(0, 83, 320, 1))
        line.backgroundColor = UIColorFromHex(0xcccccc)
        
        self.contentView.addSubview(hilightView)
        self.contentView.addSubview(titleLabel)
        self.contentView.addSubview(siteLabel)
        self.contentView.addSubview(favoritLabel)
        self.contentView.addSubview(line)
    }
    
    func reload(article:Article){
        self.article = article
        
        titleLabel.text = article.title
        siteLabel.frame = CGRectMake(13.5, 60, 227, 10)
        siteLabel.text = article.site!.name
        siteLabel.sizeToFit()
        siteLabel.frame.size.width += 10
        siteLabel.frame.size.height += 4
        siteLabel.textColor = article.site!.color
        siteLabel.layer.borderColor = article.site!.color.CGColor
        
        favoritLabel.text = AloeDateUtil.dateToString(article.publishedAt, format: "MM.dd")
    }
    
    func tap(){
        AloeTweenChain().add(0.1, ease: AloeEase.OutCirc, progress:{ (val) -> () in
            self.hilightView.alpha = val
        }).wait(0.5).call({ () -> () in
            self.hilightView.alpha = 0
        }).execute()
    }

}
