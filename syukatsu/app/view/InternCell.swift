//
//  InternCell.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/27.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class InternCell: UITableViewCell {

    var intern:Intern?
    let imageV:UIImageView = UIImageView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 190))
    let colorView:UIView = UIView()
    let titleLabel:UILabel = UILabel(frame: CGRectMake(10, 190-50+5, AloeDeviceUtil.windowWidth()-20, 40))
    let dateLabel:UILabel = UILabel(frame: CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50, 30))
    let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.Gray)
    let labelBg:UIView = UIView(frame: CGRectMake(0, 190-50, AloeDeviceUtil.windowWidth(), 50))
    let dateLabelBg:UIView = UIView()
    let hilightView:UIView = UIView(frame: CGRectMake(0, 0, AloeDeviceUtil.windowWidth(), 190))
    let siteLabel = UILabel(frame: CGRectMake(10, 15, 320, 20))

    func setup(){
        self.selectionStyle = UITableViewCellSelectionStyle.None
        let view:UIView = self.contentView
        imageV.contentMode = UIViewContentMode.ScaleAspectFill
        imageV.clipsToBounds = true
        indicator.frame = imageV.frame
        colorView.frame = imageV.frame
        titleLabel.textColor = UIColorFromHex(0xffffff)
        titleLabel.font = UIFont.boldSystemFontOfSize(16)
        titleLabel.numberOfLines = 2
        labelBg.backgroundColor = UIColor.blackColor()
        labelBg.alpha = 0.5
        dateLabel.font = UIFont.boldSystemFontOfSize(16.0)
        dateLabel.textColor = UIColorFromHex(0x252525)
        dateLabel.textAlignment = NSTextAlignment.Center
        dateLabelBg.frame = CGRectMake(AloeDeviceUtil.windowWidth()-50, 15, 50 + 15, 30)
        dateLabelBg.backgroundColor = UIColor.whiteColor()
        dateLabelBg.alpha = 0.8
        dateLabelBg.layer.cornerRadius = 15
        siteLabel.textAlignment = NSTextAlignment.Center
        siteLabel.textColor = UIColor.whiteColor()
        siteLabel.font = UIFont.systemFontOfSize(14)
        siteLabel.layer.cornerRadius = 3
        siteLabel.clipsToBounds = true
        
        hilightView.backgroundColor = UIColor.whiteColor()
        hilightView.alpha = 0
        hilightView.userInteractionEnabled = false
        
        view.addSubview(indicator)
        view.addSubview(imageV)
        view.addSubview(colorView)
        view.addSubview(dateLabelBg)
        view.addSubview(dateLabel)
        view.addSubview(labelBg)
        view.addSubview(titleLabel)
        view.addSubview(dateLabel)
        view.addSubview(siteLabel)
        view.addSubview(hilightView)
    }
    
    func reload(intern:Intern){
        self.intern = intern
        colorView.hidden = true
        titleLabel.text = intern.title
        dateLabel.text = AloeDateUtil.dateToString(intern.publishedAt, format: "M.dd")
        
        siteLabel.frame = CGRectMake(10, 15, 320, 20)
        siteLabel.text = intern.site.name
        siteLabel.sizeToFit()
        siteLabel.frame.size.width += 6
        siteLabel.frame.size.height += 3
        siteLabel.backgroundColor = intern.site.color
        
        if(!intern.hasImage()){
            colorView.hidden = false
            colorView.backgroundColor = UIColor(hue: 0.5, saturation: 0.5, brightness: 0.5, alpha: 1.0)
            return
        }
        
        imageV.image = nil
        imageV.alpha = 0
        imageV.backgroundColor = UIColor.whiteColor()
        imageV.transform = CGAffineTransformMakeScale(0.9, 0.9)
        indicator.startAnimating()
        AloeImageCache.loadImage(intern.imageUrl, callback: { (image, url, useCache) -> () in
            if(url != self.intern!.imageUrl){
                return
            }
            self.imageV.image = image
            self.indicator.stopAnimating()
            if(useCache){
                self.imageV.alpha = 1.0
                self.imageV.transform = CGAffineTransformMakeScale(1.0, 1.0)
                return
            }
            AloeTween.doTween(0.2, ease: AloeEase.OutQuint, progress: { (val) -> () in
                self.imageV.alpha = val
                self.imageV.transform = CGAffineTransformMakeScale(0.9+(val*0.1), 0.9+(val*0.1))
            })
            }) {
                (error) -> () in
                println("image failed")
                println("\(self.intern!.imageUrl)")
                self.imageV.backgroundColor = SUtil.randColor()
                self.indicator.stopAnimating()
                
                AloeTween.doTween(0.2, ease: AloeEase.OutQuint, progress: { (val) -> () in
                    self.imageV.alpha = val
                    self.imageV.transform = CGAffineTransformMakeScale(0.9+(val*0.1), 0.9+(val*0.1))
                })
//                self.imageV.backgroundColor
        }
    }
    
    func tap(){
        AloeTween.doTween(0.3, ease: AloeEase.InQuart) { (val) -> () in
            let fromAlpha:CGFloat = 0.8
            self.hilightView.alpha = fromAlpha - (val * fromAlpha)
        }
    }
    
}
