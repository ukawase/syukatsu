//
//  Contents.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Contents: NSObject, UINavigationControllerDelegate, NativeAdViewDelegate, NativeBannerDelegate {
   
    let navi = UINavigationController()
    private let header = Header()
    private let nativeAdView = NativeAdView()
    private var settingVc:SubscriptionListViewController = SubscriptionListViewController()
    
    func setup(vc:BaseViewController){
        navi.setNavigationBarHidden(true, animated: false)
        navi.pushViewController(vc, animated: false)
        navi.view.frame.origin.y = 0
        navi.delegate = self
        
        settingVc.setup { () -> () in
            println("hide")
            Model.sharedInstance.staticLoad()
            self.reload()
        }
        
        nativeAdView.delegate = self
        
        header.setup()
        navi.view.addSubview(nativeAdView.getView())
        
        navi.view.addSubview(header.getView())
        
        let swipeReco = UISwipeGestureRecognizer(target: self, action: "swipeRight")
        swipeReco.direction = UISwipeGestureRecognizerDirection.Right
        navi.view.addGestureRecognizer(swipeReco)
        
        self.getView().hidden = true
    }
    
    func swipeRight(){
        println("swipeRight")
        if navi.viewControllers.count > 1{
            navi.popViewControllerAnimated(true)
        }
    }
    
    func think(){
        let vc:ThinkListViewController = navi.viewControllers[0] as ThinkListViewController
        vc.setHeader(header)
    }
    
    func news(){
        let settingButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        settingButton.frame = CGRectMake(320-44, 20, 44, 44)
        settingButton.setImage(UIImage(named: "settingButton"), forState: UIControlState.Normal)
        settingButton.addTarget(self, action: "showSetting", forControlEvents: UIControlEvents.TouchUpInside)
        header.getView().addSubview(settingButton)
        
        let nekoButton:UIButton = UIButton.buttonWithType(UIButtonType.Custom) as UIButton
        nekoButton.frame = CGRectMake(5, 20, 44, 44)
        nekoButton.setImage(UIImage(named: "neko"), forState: UIControlState.Normal)
        nekoButton.addTarget(self, action: "showShimane", forControlEvents: UIControlEvents.TouchUpInside)
        header.getView().addSubview(nekoButton)
    }
    
    func showSetting(){
        settingVc.reload()
        
        SUtil.rootViewController().view.addSubview(settingVc.view)
        settingVc.show()
    }
    
    func showShimane(){
        println("showShimane")
    }
    
    func getView()->UIView{
        return navi.view
    }
    
    func show(){
        self.getView().hidden = false
        
        NativeBannerView.sharedInstance.delegate = self
        navi.view.insertSubview(NativeBannerView.sharedInstance.getView(), belowSubview: header.getView())
        AloeThreadUtil.wait(0.2, block: { () -> () in
            NativeBannerView.sharedInstance.tryShow()
        })
    }
    
    func hide(){
        self.getView().hidden = true
        navi.popToRootViewControllerAnimated(false)
        header.getView().alpha = 1 // static
        NativeBannerView.sharedInstance.staticHide()
    }
    
    func reload(){
        let vc:BaseViewController = navi.viewControllers[0] as BaseViewController
        vc.reload()
    }
    
    // MARK: UINavigationControllerDelegate
    
    func navigationController(navigationController: UINavigationController, willShowViewController viewController: UIViewController, animated: Bool) {
        let v:BaseViewController = viewController as BaseViewController
        header.setTitle(v.getHeaderTitle())
        
        if !animated{
            return
        }
                
        if navi.viewControllers.count == 2{
            header.hide()
            nativeAdView.hide()
            NativeBannerView.sharedInstance.hide()
        }
        
        if navi.viewControllers.count == 1{
            header.show()
            nativeAdView.tryShow()
            AloeEventUtil.dispatchGlobalEvent(ON_SCROLL_UP_EVENT)
        }
    }
    
    // MARK:NativeAdViewDelegate
    func onTapNativeAd(nativeAd: NativeAd) {
        let vc = AppViewController()
        vc.setup(nativeAd)
        
        navi.pushViewController(vc, animated: true)
    }
    
    // MARK: NativeBannerDelegate
    func onSelectNativeAd(nativeAd: NativeAd) {
        let vc = AppViewController()
        vc.setup(nativeAd)
        
        navi.pushViewController(vc, animated: true)
    }
    
    // dummy
    func onHideBannerAd(index: Int) {}
    
}
