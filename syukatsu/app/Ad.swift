//
//  Ad.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation

private var chkController:ChkController = ChkController()
private var nativeAdList:NativeAdList = NativeAdList.createDummy()
private var iconAdList:NativeAdList = NativeAdList.createDummy()
private var isReady_:Bool = false

class Ad{
    
    private class ChkDelegate:NSObject, ChkControllerDelegate{
        
        func chkControllerDataListWithSuccess(data: [NSObject : AnyObject]!) {
            
            
            
            var list:[ChkRecordData] = []
            var allList:[ChkRecordData] = []
            for row in chkController.dataList{
                let chkData:ChkRecordData = row as ChkRecordData
                if chkData.isInstalled {
                    continue
                }
                allList.append(chkData)
                if(chkData.hasNativeBanner){
                    list.append(chkData)
                }
            }
            
            nativeAdList = NativeAdList(array: list)
            iconAdList = NativeAdList(array: allList)
            isReady_ = (nativeAdList.count() > 0)
            
            println("chkControllerDataListWithSuccess")
            if(chkController.hasNextData){
//                subThread({ () -> () in
                    chkController.requestDataList()
//                })
            }else{
                AloeEventUtil.dispatchGlobalEvent(ON_NATIVE_BANNER_AD_LOADED)
            }
        }
        
        func chkControllerDataListWithNotFound(data: [NSObject : AnyObject]!) {
            
        }
        
        func chkControllerDataListWithError(error: NSError!) {
            
        }
    }
    
    // http://qiita.com/susieyy/items/acb3bc80a1dafe64cffd
    class var sharedInstance : Ad {
    struct Static {
        static let instance : Ad = Ad()
        }
        return Static.instance
    }
    
    private let chkDelegate:ChkDelegate = ChkDelegate()
    
    private init(){
        chkController = ChkController(delegate: chkDelegate)
    }
    
    func getNativeAd()->NativeAd{
        
        let r:Int = Int(arc4random_uniform(UInt32(iconAdList.count())))
        return iconAdList.get(r)
    }
    
    func getNativeAdList()->NativeAdList{
        return nativeAdList
    }
    
    func getIconAdList()->NativeAdList{
        return iconAdList
    }
    
    func reload(){
//        subThread({ () -> () in
//            chkController.requestDataList()
//        })
        chkController.requestDataList()
    }
    
    func isReady()->Bool{
        return isReady_
    }
    
}