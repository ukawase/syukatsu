//
//  Mediator.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Mediator: NSObject, TabDelegate {
   
    private var contentsList:[Contents] = []
    private let tab:Tab = Tab()
    private let contentView = UIView(frame: CGRectMake(0, 0, 320, AloeDeviceUtil.windowHeight()))
    
    func setup(window:UIWindow){
        UIApplication.sharedApplication().setStatusBarStyle(UIStatusBarStyle.LightContent, animated: false)
        
        let rootVc = RootViewController()
        SUtil.setup(rootVc)
        NativeBannerView.sharedInstance
        
        println("model:\(UIDevice.currentDevice().model)")
        println("idiom:\(UIDevice.currentDevice().userInterfaceIdiom)")
        
        if !UIDevice.currentDevice().model.hasPrefix("iPad")
        {
            AloeUtil.setup()
        }else{
            println("ipad skip")
        }
        
        self.setContents()
        
        tab.setup()
        tab.delegate = self
        rootVc.view.addSubview(tab.getView())
        window.rootViewController = rootVc
        
        if Model.sharedInstance.isInitialized(){
            println("staticLoad")
            Model.sharedInstance.staticLoad()
            for index in 0..<self.contentsList.count{
                let contents = self.contentsList[index]
                contents.reload()
            }
            AloeUtil.activeApp()
        }
        
        self.reload()
        
        // events
        AloeEventUtil.addGlobalEventListener(self, selector: "onScrollUp", name: ON_SCROLL_UP_EVENT)
        AloeEventUtil.addGlobalEventListener(self, selector: "onScrollDown", name: ON_SCROLL_DOWN_EVENT)
    }
    
    private func setContents(){
        SUtil.rootViewController().view.addSubview(contentView)
        
        let newsContents = Contents()
        let newsVc:NewsListViewController = NewsListViewController()
        newsVc.setup()
        newsContents.setup(newsVc)
        newsContents.news()
        contentView.addSubview(newsContents.getView())
        contentsList.append(newsContents)
        
        let thinkContents = Contents()
        let thinkVc:ThinkListViewController = ThinkListViewController()
        thinkVc.setup()
        thinkContents.setup(thinkVc)
        thinkContents.think()
        contentView.addSubview(thinkContents.getView())
        contentsList.append(thinkContents)
        
        let naverContents = Contents()
        let naverVc = NaverViewController()
        naverVc.setup()
        naverContents.setup(naverVc)
        contentView.addSubview(naverContents.getView())
        contentsList.append(naverContents)
        
        let tweetContents = Contents()
        let tweetVc:TweetListViewController = TweetListViewController()
        tweetVc.setup()
        tweetContents.setup(tweetVc)
        contentView.addSubview(tweetContents.getView())
        contentsList.append(tweetContents)
        
        let internContents = Contents()
        let internVc = InternListViewController()
        internVc.setup()
        internContents.setup(internVc)
        contentView.addSubview(internContents.getView())
        contentsList.append(internContents)
        
        // initial
        newsContents.show()
        SUtil.trackScreen("initial")
    }
    
    func reload(){
        SUtil.rootViewController().showLoading()
        Model.sharedInstance.reloadModel { () -> () in
            println("reloaded")
            for index in 0..<self.contentsList.count{
                let contents = self.contentsList[index]
                contents.reload()
            }
            self.tab.reload()
            AloeUtil.activeApp()
            self.tryShowReview()
            Ad.sharedInstance.reload()
            
            SUtil.rootViewController().hideLoading()
        }
    }
    
    func willEnterForeground(){
        self.reload()
    }
    
    func didEnterBackground(){
//        let alert:UIAlertController = UIAlertController(title: "テスト", message: "テスト", preferredStyle: UIAlertControllerStyle.Alert)
//        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
//        SUtil.rootViewController().presentViewController(alert, animated: true, completion: nil)
        
        println("didEnterBackground")
    }
    
    func memoryWarning(){
        AloeImageCache.clear()
    }
    
    func onScrollUp(){
        tab.show()
    }
    
    func onScrollDown(){
        tab.hide()
    }
    
    func tryShowReview(){
        if AloeDeviceUtil.osVersion() < 8.0{
            println("7系")
            return
        }
        
        let noReviewKey:String = "noReviewKey"
        
        if(NSUserDefaults.standardUserDefaults().boolForKey(noReviewKey)){
            println("noreview")
            return
        }
        
        let activeCount = AloeUtil.activeAppCount()
        println("activeCount:\(activeCount)")
        if(!(activeCount == 5 || activeCount == 20 || activeCount == 50)){
            return
        }
        
        AloeUtil.activeApp()
        
        let title:String = "ご利用ありがとうございます"
        let message:String = "使い心地はいかがでしょうか。\nご意見ご要望・叱咤激励等\nAppStoreにてレビューを頂ければ幸いです。"
        let alert:UIAlertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        
        alert.addAction(UIAlertAction(title: "レビューする", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            let url:String = "https://itunes.apple.com/jp/app/id954170365?l=ja&ls=1&mt=8"
            AloeUtil.openBrowser(url)
            let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            ud.setBool(true, forKey: noReviewKey)
            ud.synchronize()
        }))
        alert.addAction(UIAlertAction(title: "あとで", style: UIAlertActionStyle.Default, handler: { (action) -> Void in
            
        }))
        alert.addAction(UIAlertAction(title: "レビューしない", style: UIAlertActionStyle.Cancel, handler: { (action) -> Void in
            let ud:NSUserDefaults = NSUserDefaults.standardUserDefaults()
            ud.setBool(true, forKey: noReviewKey)
            ud.synchronize()
        }))
        
        SUtil.rootViewController().presentViewController(alert, animated: true) { () -> Void in
            
        }
    }
    
    // MARK: TabDelegate
    
    func onSelect(index: Int) {
        for i in 0..<contentsList.count{
            let c:Contents = contentsList[i]
            c.hide()
        }
        
        let contents:Contents = contentsList[index]
        contents.show()
    }
    
}
