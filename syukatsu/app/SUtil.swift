//
//  SUtil.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

private let sUtilTwDateFormatter:NSDateFormatter = NSDateFormatter()
private var rootVc:RootViewController?
public var GlobalMenuTy:CGFloat = 0
private let SUTIL_NOAD_KEY = "noAdKey"

class SUtil: NSObject {
   
    class func setup(vc:RootViewController){
        rootVc = vc
        sUtilTwDateFormatter.locale = NSLocale(localeIdentifier: "en_US")
        sUtilTwDateFormatter.dateFormat = "EEE MMM dd HH:mm:ss Z yyyy"
        
        let gai:GAI = GAI.sharedInstance()
        gai.trackUncaughtExceptions = true
        
        gai.dispatchInterval = 20
        gai.logger!.logLevel = GAILogLevel.None
        gai.trackerWithTrackingId(GA_TRACKING_ID)
    }
    
    class func trackScreen(screenName:String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.set(kGAIScreenName, value: screenName)
        tracker.send(GAIDictionaryBuilder.createAppView().build())
    }
    
    class func trackEvent(category:String, action:String, label:String){
        let tracker = GAI.sharedInstance().defaultTracker
        tracker.send(GAIDictionaryBuilder.createEventWithCategory(category, action: action, label: label, value: 0).build())
    }
    
    class func rootViewController()->RootViewController{
        return rootVc!
    }
    
    class func contentsFrame()->CGRect{
        return CGRectMake(0, 0, 320, AloeDeviceUtil.windowHeight())
    }
    
    class func headerContentsFrame()->CGRect{
        return CGRectMake(0, 64, 320, AloeDeviceUtil.windowHeight())
    }
    
    class func toDateFromTweet(str:String)->NSDate{
        return sUtilTwDateFormatter.dateFromString(str)!
    }
    
    class func isNoAd()->Bool{
        return NSUserDefaults.standardUserDefaults().boolForKey(SUTIL_NOAD_KEY)
    }
    
    class func purchasedNoAd(){
        let ud = NSUserDefaults.standardUserDefaults()
        ud.setBool(true, forKey: SUTIL_NOAD_KEY)
        ud.synchronize()
    }
    
    class func randColor()->UIColor{
        let h:CGFloat = CGFloat(arc4random_uniform(360)) / 360
        let s:CGFloat = 0.59
        let b:CGFloat = 0.56
        let bgColor:UIColor = UIColor(hue: h, saturation: s, brightness: b, alpha: 1.0)
        return bgColor
    }
}
