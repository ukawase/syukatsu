//
//  AppConfig.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

// params
let TAB_HEIGHT:CGFloat = 49.0
let API_ENDPOINT:String = "http://syukatu.nowhappy.info/api"
let NATIVE_AD_SHOW_RATE:Int = 6
let NATIVE_AD_LIST_RATE:Int = 20
let PURCHASE_NOAD:String = "com.aloeproject.syukatsu.noad"

// Analytics
let GA_TRACKING_ID = "UA-58034393-1"

// Imobile
let IMOBILE_PUBLISHER_ID = "9873"
let IMOBILE_MEDIA_ID = "137672"
let IMOBILE_SPOT_ID_ICON = "349756"
let IMOBILE_SPOT_ID_BANNER = "350750"

// parse
let PARSE_APPLICATION_ID = "eS4vi4NJpSuUgCPhZNlk2ZTwUK3JbKumMPfE4xAZ"
let PARSE_CLIENT_KEY = "dw4NWCbdRY4b8TwouKxuHWzmc6XGPPL7P2l8a3Pe"

// Event
let ON_SCROLL_UP_EVENT:String = "ON_SCROLL_UP_EVENT"
let ON_SCROLL_DOWN_EVENT:String = "ON_SCROLL_DOWN_EVENT"
let ON_NATIVE_BANNER_AD_LOADED:String = "ON_NATIVE_BANNER_AD_LOADED"

// UserDefaultKeys
let tryNativeAdNumKey:String = "tryNativeAdNumKey"

class AppConfig: NSObject {
   
}
