//
//  Intern.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/27.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Intern: AloeModel {
   
    let title:String
    let site:Site
    let imageUrl:String
    let linkUrl:String
    let publishedAt:NSDate
    let published_at:Int
    
    init(nsdic:NSDictionary){
        title = nsdic.objectForKey("title") as String
        imageUrl = nsdic.objectForKey("image_url") as String
        linkUrl = nsdic.objectForKey("link_url") as String
        let published:String = nsdic["created_at"] as String
        
        published_at = published.toInt()!
        publishedAt = NSDate(timeIntervalSince1970: NSString(string: published).doubleValue);
        
        site = Site()
        site.name = nsdic.objectForKey("name") as String
        site.color = UIColorFromHexString(nsdic.objectForKey("color") as String)
    }
    
    func hasImage()->Bool{
        return countElements(imageUrl) != 0
    }
    
}
