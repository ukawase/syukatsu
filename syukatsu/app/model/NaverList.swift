//
//  NaverList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NaverList: AloeModelList {
   
    override init() {
        super.init()
    }
    
    override func get(index: Int) -> Naver {
        return super.get(index) as Naver
    }
    
    override init(nsarray:NSArray){
        super.init()
        
        for dic in nsarray{
            let n = Naver(nsdic: dic as NSDictionary)
            self.add(n)
        }
    }
    
}
