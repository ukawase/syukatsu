//
//  Article.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Article: AloeModel {
    
    var title:String = ""
    var publishedAt:NSDate = NSDate()
    var published_at:Int = 0
    var linkUrl:String = ""
    var site:Site?
    var favorits:Int = 1234
    var imageUrl:String = ""
   
    override init(){
        
    }
    
    init(nsdic:NSDictionary){
        super.init()
        
        title = nsdic["title"] as String
        let published:String = nsdic["published_at"] as String
        published_at = published.toInt()!
        publishedAt = NSDate(timeIntervalSince1970: NSString(string: published).doubleValue);
        linkUrl = nsdic["link_url"] as String
        site = Site()
        site!.name = nsdic["site_name"] as String
        site!.color = UIColorFromHexString(nsdic["color"] as String)
    }
    
}
