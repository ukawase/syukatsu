//
//  InternList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/27.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class InternList: AloeModelList {
    
    override init(){
        super.init()
    }
    
    override init(nsarray: NSArray) {
        super.init()
        
        for dic in nsarray{
            let intern = Intern(nsdic: dic as NSDictionary)
            self.add(intern)
        }
        
    }
    
    override func get(index: Int) -> Intern {
        return super.get(index) as Intern
    }
    
}
