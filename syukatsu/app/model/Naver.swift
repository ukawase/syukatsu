//
//  Naver.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/28.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Naver: AloeModel {
   
    let title:String
    let linkUrl:String
    let imageUrl:String
    let views:Int
    let createdAt:Int
    
    init(nsdic:NSDictionary){
        title = nsdic["title"] as String
        linkUrl = nsdic["link_url"] as String
        imageUrl = nsdic["image_url"] as String
        views = (nsdic["view"] as String).toInt()!
        createdAt = (nsdic["created_at"] as String).toInt()!
        
        super.init()
    }
    
    func hasImage()->Bool{
        return countElements(imageUrl) != 0
    }
    
}
