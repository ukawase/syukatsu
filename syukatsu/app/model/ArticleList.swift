//
//  ArticleList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class ArticleList: AloeModelList {
    
    override init(){
        super.init()
    }
    
    override init(nsarray: NSArray) {
        super.init()
        for dic in nsarray{
            let a = Article(nsdic: dic as NSDictionary)
            self.add(a)
        }
    }
    
    override func get(index: Int) -> Article {
        return super.get(index) as Article
    }

}
