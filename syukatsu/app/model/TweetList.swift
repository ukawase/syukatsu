//
//  TweetList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/26.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class TweetList: AloeModelList {
   
    override init(nsarray:NSArray){
        super.init()
        for nsdic in nsarray{
            let t = TweetModel(nsdic: nsdic as NSDictionary)
            self.add(t)
        }
    }
    
    override func get(index: Int) -> TweetModel {
        return super.get(index) as TweetModel
    }
    
}
