//
//  SiteList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class SiteList: AloeModelList {
   
    override init(nsarray: NSArray) {
        super.init()
        
        for dic in nsarray{
            let s = Site(nsdic: dic as NSDictionary)
            self.add(s)
        }
        
    }
    
    override func get(index: Int) -> Site {
        return super.get(index) as Site
    }

}
