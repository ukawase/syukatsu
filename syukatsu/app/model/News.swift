//
//  News.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class News: AloeModel {
   
    var title:String = ""
    var imageUrl:String = ""
    var linkUrl:String = ""
    var siteId:Int = 0
    var site:Site?
    var publishedAt:Int = 0
    
    init(nsdic:NSDictionary){
        title = nsdic.objectForKey("title") as String
        imageUrl = nsdic["image_url"] as String
        linkUrl = nsdic["link_url"] as String
        siteId = (nsdic["news_site_id"] as String).toInt()!
        publishedAt = (nsdic["published_at"] as String).toInt()!
    }
    
    func hasImage()->Bool{
        return countElements(imageUrl) != 0
    }
    
    override init() {
        
    }
    
    class func createDummy()->News{
        let news = News()
        news.title = "知らなきゃ損！第一印象を変える”就活メイク”"
        news.imageUrl = "http://rr.img.naver.jp/mig?src=http%3A%2F%2Fimgcc.naver.jp%2Fkaze%2Fmission%2FUSER%2F20121102%2F12%2F1279702%2F17%2F144x240x5a8e5cd3ba58b6454dc39153.jpg%2F300%2F600&twidth=95&theight=95&qlt=80&res_format=jpg&op=sc"
        news.site = Site.createDummy()
        
        return news
    }
    
}
