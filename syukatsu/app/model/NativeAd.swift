//
//  NativeAd.swift
//  nekosoku
//
//  Created by kawase yu on 2014/10/02.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import Foundation



class NativeAd:AloeModel{

    let title:String
    let linkUrl:String
    let appStoreId:String
    let iconUrl:String
    var bannerUrl:String?
    let content:String
    let detail:String
    
    init(chkData:ChkRecordData){
        title = chkData.title
        linkUrl = chkData.linkUrl
        appStoreId = chkData.appStoreId
        iconUrl = chkData.imageIcon
        content = chkData.description
        detail = chkData.detail
        bannerUrl = chkData.hasNativeBanner ? chkData.nativeBannerUrl : nil
        
        if(!chkData.hasNativeBanner){
            return
        }
        bannerUrl = chkData.nativeBannerUrl
    }
    
    init(title:String, appId:String, iconUrl:String, linkUrl:String, content:String){
        self.title = title
        self.appStoreId = appId
        self.iconUrl = iconUrl
        self.linkUrl = linkUrl
        self.content = content
        self.detail = ""
    }
    
    func hasBanner()->Bool{
        return (bannerUrl != nil)
    }
    
}