//
//  NewsList.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class NewsList: AloeModelList {
    
    override init() {
        super.init()
    }
    
    override init(nsarray: NSArray) {
        super.init()
        for dic in nsarray{
            let n = News(nsdic: dic as NSDictionary)
            self.add(n)
        }
    }
    
    func largeNews()->News?{
        for index in 0..<self.count(){
            let n = self.get(index)
            if n.hasImage(){
                return n
            }
        }
        
        return nil
    }
    
    override func get(index: Int) -> News {
        return super.get(index) as News
    }
   
    class func createDummy()->NewsList{
        
        let newsList = NewsList()
        
        for index in 0..<20{
            let news:News = News.createDummy()
            newsList.add(news)
        }
        
        return newsList
    }
    
}
