//
//  Site.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class Site: AloeModel {
   
    var name:String = ""
    var color:UIColor = UIColor(hue: 30/360, saturation: 0.56, brightness: 0.63, alpha: 1)
    var id:Int = 0
    var isSubscription:Bool = true
    
    override init(){
        
    }
    
    init(nsdic:NSDictionary){
        id = (nsdic["id"] as String).toInt()!
        name = nsdic["name"] as String
        color = UIColorFromHexString(nsdic["color"] as String)
        super.init()
    }
    
    class func createDummy()->Site{
        let site = Site()
        site.name = "NAVERまとめ"
        
        return site
    }
    
}
