//
//  TweetModel.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/26.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit

class TweetModel: AloeModel {
    
    let id:Int
    let name:String
    let txt:String
    let createdDate:NSDate
    let screenName:String
    let iconUrl:String
    let linkUrl:String
    
    init(nsdic:NSDictionary){
        id = nsdic.objectForKey("id") as Int
        txt = nsdic.objectForKey("text") as String
        createdDate = SUtil.toDateFromTweet(nsdic.objectForKey("created_at") as String)
        
        let user:NSDictionary = nsdic.objectForKey("user") as NSDictionary
        screenName = user.objectForKey("screen_name") as String
        iconUrl = user.objectForKey("profile_image_url") as String
        linkUrl = "https://twitter.com/" + screenName + "/status/" + String(id) + "?lang=ja"
        name = user.objectForKey("name") as String
        
        super.init()
    }
    
    
   
}
