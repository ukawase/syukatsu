//
//  AppDelegate.swift
//  syukatsu
//
//  Created by kawase yu on 2014/12/25.
//  Copyright (c) 2014年 aloeproject. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics
import TwitterKit
import StoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    private let mediator = Mediator()

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        
        // crashlytics
        Fabric.with([Crashlytics(), Twitter()])
        
        // parse
        Parse.enableLocalDatastore()
        Parse.setApplicationId(PARSE_APPLICATION_ID, clientKey: PARSE_CLIENT_KEY)
        PFAnalytics.trackAppOpenedWithLaunchOptionsInBackground(launchOptions, block: { (b, error) -> Void in
            println("b:\(b)")
            if error != nil{
                println("error:\(error)")
            }
        })
        
        UIApplication.sharedApplication().applicationIconBadgeNumber = -1
        self.window = UIWindow(frame: UIScreen.mainScreen().bounds)
        // Override point for customization after application launch.
        self.window!.backgroundColor = UIColor.whiteColor()
        self.window!.makeKeyAndVisible()
        
        mediator.setup(self.window!)
        
        if application.respondsToSelector("isRegisteredForRemoteNotifications"){
            var types: UIUserNotificationType = UIUserNotificationType.Badge |
                UIUserNotificationType.Alert |
                UIUserNotificationType.Sound
            
            var settings: UIUserNotificationSettings = UIUserNotificationSettings( forTypes: types, categories: nil )
            
            application.registerUserNotificationSettings( settings )
            application.registerForRemoteNotifications()
        }else{
            application.registerForRemoteNotificationTypes(.Badge | .Sound | .Alert)
        }
        
        return true
    }

    func applicationDidReceiveMemoryWarning(application: UIApplication) {
        mediator.memoryWarning()
    }
    
    func application( application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData ) {
        
        var characterSet: NSCharacterSet = NSCharacterSet( charactersInString: "<>" )
        
        var deviceTokenString: String = ( deviceToken.description as NSString )
            .stringByTrimmingCharactersInSet( characterSet )
            .stringByReplacingOccurrencesOfString( " ", withString: "" ) as String
        
        println( deviceTokenString )
        
        let url:String = Api.url("registerToken.php")
        println(url)
        let c:AloePostCommand = AloePostCommand(url: url, params: ["device_token":deviceTokenString])
        c.setCompleteBlock { (data) -> () in
            println("registered")
        }
        c.setFailBlock { (error) -> () in
            println("failRegisterd")
        }
        c.execute()
    }
    
    func applicationWillResignActive(application: UIApplication) {
        
    }

    func applicationDidEnterBackground(application: UIApplication) {
        ChkApplicationOptional.applicationDidEnterBackground(application)
        mediator.didEnterBackground()
    }

    func applicationWillEnterForeground(application: UIApplication) {
        UIApplication.sharedApplication().applicationIconBadgeNumber = -1
        ChkApplicationOptional.applicationWillEnterForeground()
        mediator.willEnterForeground()
    }

    func applicationDidBecomeActive(application: UIApplication) {
        
    }

    func applicationWillTerminate(application: UIApplication) {

    }


}

