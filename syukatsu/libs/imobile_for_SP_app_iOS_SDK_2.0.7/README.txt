imobile_for_SP_app_iOS_SDK_2.0.7 - README

【目次】
1. 配布物
2. 導入方法
3. 旧バージョンからのバージョンアップ時注意事項
4. 更新履歴
5. 本バージョンでの修正点詳細
6. 各種問い合わせ先


=======================================
1. 配布物
=======================================
・ImobileSdkAds.framework
  (/lib)

・Swift組み込む用、Bridging-Header
  (/Swift/ImobileSdkAds-Bridging-Header.h)

・Swift組み込む用、Bridging-Header(AdMobMediation Adapter 利用時)
  (/Swift/ImobileSdkAds-Adapter-Bridging-Header.h)

・サンプルプログラム
  (/sample/ImobileSdkAdsSample/ImobileSdkAdsSample.xcodeproj)

・libAdapterIMobile.a
  AdMobMediationを利用する為のアダプタです。

・README.txt (本テキストファイル)

=======================================
2. 導入方法
=======================================
導入方法につきましては、以下の設定資料を参照してください。
 ■ Objective-C
  「i-mobile for SP Application　広告表示SDK　Version.2 iOS 設定資料」
   URL:https://sppartner.i-mobile.co.jp/downloads/sdk/i-mobile_SDK_manual_for_iOS.pdf
 ■ Swift
  「i-mobile for SP Application 広告表示SDK Version.2 iOS (Swift) 設定資料」
   URL:https://sppartner.i-mobile.co.jp/downloads/sdk/i-mobile_SDK_manual_for_Swift.pdf

=======================================
3. 旧バージョンからのバージョンアップ時注意事項
=======================================
Verβ,1.0.2からのバージョンアップの場合
広告表示時のスキップ数が管理画面で設定できるようになりました。
そのため、[ImobileSdkAds setSpotSkipCount]メソッドによる、
スキップ数の設定ができなくなりました。
コード上で設定しても反映されません。
現在利用している場合、お手数ですが管理画面からスポットの設定をしてください。

=======================================
4. 更新履歴
=======================================
2013/11/22  β          βリリース
2013/12/04  1.0.2       正式リリース
2014/01/22  1.1.0       管理画面で設定された間隔時間、スキップ数を反映可能に変更
                        広告の向きを指定する機能を追加
2014/04/24  ウォールβ  ウォール広告対応βリリース
2014/05/12  1.3.0       ウォール広告対応正式リリース
2014/05/22  バナーβ    バナー広告、iPad広告対応βリリース
2014/06/02  1.3.1       バナー広告、iPad広告対応正式リリース
2014/06/05  1.3.2       ウォール広告スポットの高度な設定「フィルタ設定バージョン」対応
2014/06/12  アイコンβ  アイコン広告対応βリリース
2014/06/19  2.0.0       アイコン広告対応正式リリース
2014/07/01  2.0.1       AdMobメディエーションアダプタ追加
2014/07/22  2.0.2       不具合修正
2014/08/06  2.0.3       アイコン広告の表示形式設定パラメータを拡張
2014/08/21  2.0.4       不具合修正
                        デリゲートメソッド「imobileSdkAdsSpotDidShow」追加
2014/09/18  2.0.5       不具合修正
                        Swift用のBridging-Headerを追加
2014/11/17  β          βリリース  テキストポップアップ広告対応
                        XCode 5.x「framework not found Metal for architecture」対応
2014/11/20  2.0.6       テキストポップアップ広告対応正式リリース
2014/12/11  2.0.7       iPhone6,6Plusの横幅いっぱいにバナー広告を表示するモードを追加
			AdMobメディエーションアダプタの広告サイズ：kGADAdSizeSmartBannerPortrait 対応
=======================================
5. 本バージョンでの修正点詳細
=======================================
・iPhone6,6Plusの横幅いっぱいにバナー広告を表示するモードを追加しました。

　詳細、使用方法については、
  ■ Objective-C 
   「i-mobile for SP Application　広告表示SDK　Version.2 iOS 設定資料」
　  URL:https://sppartner.i-mobile.co.jp/downloads/sdk/i-mobile_SDK_manual_for_iOS.pdf

    P.23 「3.2.6 インライン広告の表示方法（デバイスの横幅に合わせて拡大表示する）」
　　を参照してください

  ■ Swift
   「i-mobile for SP Application 広告表示SDK Version.2 iOS (Swift) 設定資料」
　  URL:https://sppartner.i-mobile.co.jp/downloads/sdk/i-mobile_SDK_manual_for_Swift.pdf

    P.24 「3.2.6 インライン広告の表示方法（デバイスの横幅に合わせて拡大表示する）」
　　を参照してください

・AdMobメディエーションアダプタにおいて、広告サイズ：kGADAdSizeSmartBannerPortraitに対応しました。

=======================================
6. 各種問い合わせ先
=======================================
株式会社アイモバイル
http://i-mobile.co.jp/
